defmodule Oxpt.DoubleAuction.Host.PlayerSocket do
  defstruct [:game_id, :guest_game_id, :guest_id]

  use GenServer

  alias Cizen.{Dispatcher, Filter, Saga}
  alias Oxpt.Player.{Output, Request}

  alias Oxpt.DoubleAuction.Events.{
    UpdateState,
    UpdateStateAll,
    ChangePage,
    FetchState,
    ChangeSetting
  }

  require Filter

  @impl GenServer
  def init(%__MODULE__{} = socket) do
    %{guest_game_id: guest_game_id, guest_id: guest_id} = socket

    Dispatcher.listen(
      Filter.any([
        Filter.new(fn %UpdateStateAll{game_id: ^guest_game_id} ->
          true
        end),
        Filter.new(fn %UpdateState{game_id: ^guest_game_id, guest_id: ^guest_id} ->
          true
        end)
      ])
    )

    Dispatcher.dispatch(%FetchState{
      game_id: guest_game_id,
      guest_id: guest_id
    })

    {:ok, socket}
  end

  @impl GenServer
  def handle_info(%event_type{event: event, state: state}, socket)
      when event_type in [UpdateStateAll, UpdateState] do
    Dispatcher.dispatch(%Output{
      game_id: socket.game_id,
      guest_id: socket.guest_id,
      event: event,
      payload: %{state: state}
    })

    {:noreply, socket}
  end

  @impl GenServer
  def handle_info({:response, _, _}, state), do: {:noreply, state}

  def handle_request(%Request{event: "change page", payload: page} = request) do
    Saga.call(request.game_id, %ChangePage{page: page})
  end

  def handle_request(%Request{event: "setting", payload: payload} = request) do
    Saga.call(request.game_id, %ChangeSetting{payload: payload})
  end
end
