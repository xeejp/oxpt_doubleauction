defmodule Oxpt.DoubleAuction.Locales do
  def get do
    %{
      en: %{
        translations: %{
          variables: %{
            seller_01: "seller",
            buyer_01: "buyer",
            budget_01: "budget",
            cost_01: "cost",
            goods_01: "goods",
            send_01: "send",
            unit_01: "doller",
            price_01: "price",
            profit_01: "profit",
            selling_price_01: "selling price",
            buying_price_01: "buying price",
            selling_num_01: "selling amount",
            buying_num_01: "buying amount"
          },
          host: %{
            title_01: "Double Auction",
            stepper: %{
              instruction_page_01: "instruction",
              experiment_page_01: "experiment",
              result_page_01: "result",
              back_01: "back",
              next_01: "next"
            },
            setting: %{
              title_01: "Settings",
              send_01: "send",
              cancel_01: "cancel",
              language: %{
                title_01: "Language settings",
                en_01: "English",
                ja_01: "Japanese"
              },
              game: %{
                title_01: "Game settings",
                header_item_01: "Item",
                header_info_01: "Information",
                setting_01: "Setting",
                ex_type_setting_01: "Mode setting",
                simple_01: "Simple mode",
                real_01: "Real mode",
                custom_01: "Custom mode",
                price_base_01: "Price base",
                price_inc_01: "Price inc",
                price_max_01: "Price max",
                price_min_01: "Price min",
                apply_01: "Apply",
                edit_01: "Edit"
              },
              custom: %{
                title_01: "Custom mode setting",
                error_price_01: "The entered price is incorrect.",
                error_price_zero_01: "Enter at least 1 price.",
                error_price_number_01: "Set the price more than the number of participants.",
                demand_01: "Demand",
                supply_01: "Supply",
                players_01:
                  "Participants:{{players_num}} players, Expected number of participants:{{setting_num}} players."
              }
            },
            guest_table: %{
              id_01: "Id",
              role_01: "Role",
              state_01: "State",
              status: %{
                instruction_01: "Instruction",
                result_01: "Result",
                finished_01: "Finished",
                playing_01: "Playing"
              }
            }
          },
          guest: %{
            instruction: %{
              instruction_title_01: "Instruction",
              next_01: "next",
              back_01: "back",
              instructions_01: [
                "<p>You will trade the {{goods_01}}.</p><p>The participants are divided into {{seller_01}} and {{buyer_01}}.</p><p>You trade as much {{profit_01}} as you can.</p>",
                "<p>The {{seller_01}} should sell {{goods_01}} at as high a {{price_01}} as possible and raise a lot of {{profit_01}}.</p><p>To avoid losses, enter {{price_01}} more than {{cost_01}}.</p>",
                "<p>The {{buyer_01}} should buy {{goods_01}} at the lowest possible {{price_01}} and raise a lot of {{profit_01}}. </p><p>To avoid losses, enter {{price_01}} below {{budget_01}}.</p>",
                "<p>Proposal {{price_01}} must be entered as a positive integer. Proposal {{price_01}} can be changed as often as you like until the deal is closed.</p>",
                "<p>If you have any questions, we will accept them now.</p><p>You are not allowed to ask or confirm questions during the experiment.</p>"
              ]
            },
            experiment: %{
              your_buyer_01:
                "<p>You are {{buyer_01}}.</p><p>You can spend {{price_01}} up to {{money}} {{unit_01}} which is under your {{budget_01}}.</p>",
              your_buyer_n_01:
                "You are {{buyer_01}}.You can spend {{price_01}} up to {{money}} {{unit_01}} which is under your {{budget_01}}.",
              your_seller_01:
                "<p>You are {{seller_01}}.</p><p>You can sell with {{price_01}} over {{money}} {{unit_01}} which is over your {{cost_01}}.</p>",
              your_seller_n_01:
                "You are {{seller_01}}.You can sell with {{price_01}} over {{money}} {{unit_01}} which is over your {{cost_01}}.",
              donot_join_01: "<p>You are not joining this game.</p>",
              success_text_01:
                "<p>Execution</p><p>Your transaction was completed for {{deal}} {{unit_01}}.（Your suggestion: {{bid}} {{unit_01}})</p><p>Your {{profit_01}} is {{benefit}} {{unit_01}}.</p><p>Please wait until this game will be finished.</p>",
              buyer_suggest_01: "You are suggesting {{bid}} {{unit_01}} as a {{buyer_01}}.",
              seller_suggest_01: "You are suggesting {{bid}} {{unit_01}} as a {{seller_01}}.",
              error_pos_01: "Enter a positive integer",
              error_low_01: "Enter under {{budget_01}} {{unit_01}}.",
              error_up_01: "Enter over {{cost_01}} {{unit_01}}.",
              error_non_01: "Enter price.",
              suggest_price_01: "Suggesting price",
              your_suggestion_01: "You are suggesting {{bid}} {{unit_01}}.",
              check_bid_01: "Are you sure you want to bid?",
              check_bid_cancel_01: "Withdraw the bid. Are you sure?",
              bid_cancel_01: "Cancel bid",
              bid_01: "Bid",
              cancel_01: "Cancel",
              ok_01: "OK",
              close_01: "Close"
            },
            result: %{
              table_title_01:
                "{{buyer_01}}：{{buyer_num}}, {{seller_01}}：{{seller_num}}, Dealt: {{deals_num}} pairs.\n{{buyer_01}} max: {{max_value}} {{unit_01}}, {{seller_01}} min: {{min_value}} {{unit_01}}",
              success_price_01: "Dealt {{price_01}}",
              deals_num_title_01: "Dealt order",
              deals_num_01: "{{price_01}} order",
              graph_01: "Result chart",
              title_transition_01: "Progress of Dealt {{price_01}}.",
              title_curve_01: "Theoretical supply-demand curve",
              title_surplus_01: "Consumer surplus, producer surplus, total surplus",
              title_rank_01: "Total rank",
              title_bids_table_01: "Established price list",
              surplus_uni_01:
                "Theoretical value: Consumer surplus {{consumer_surplus}} {{unit_01}}, Producer surplus {{producer_surplus}} {{unit_01}}, Total surplus {{total_surplus}} {{unit_01}}",
              surplus_some_01:
                "Theoretical value: Consumer surplus {{consumer_surplus_min}}〜{{consumer_surplus_max}} {{unit_01}}, Producer surplus {{producer_surplus_min}}〜{{producer_surplus_max}} {{unit_01}}, Total surplus {{total_surplus_ideal}} {{unit_01}}",
              surplus_01:
                "Experimental result: Consumer surplus {{consumer_surplus}} {{unit_01}}, Producer surplus {{producer_surplus}} {{unit_01}}, Total surplus {{total_surplus}} {{unit_01}}",
              chart: %{
                surplus_01:
                  "Consumer surplus: {{consumer_surplus}} {{unit_01}}, Producer surplus: {{producer_surplus}} {{unit_01}}, Total surplus: {{total_surplus}} {{unit_01}}",
                chart_title_01: "Supply-demand curve",
                ideal_price_01:
                  "Theoretical equilibrium {{price_01}}({{min}} {{unit_01}}～{{max}} {{unit_01}})",
                number_01: "Num",
                ideal_number_01: "Theoretical equilibrium trading quantity ({{number}})",
                demand_01: "Demand",
                supply_01: "Supply",
                price_change_01: "Established {{price_01}}",
                success_order_01: "Established order"
              },
              rank: %{
                rank_01: "rank(total)",
                rank_seller_01: "rank(seller)",
                rank_buyer_01: "rank(buyer)",
                key_01: "id",
                profit_01: "profit"
              },
              agreed_price: %{
                board: "Board",
                table: "Table"
              }
            }
          },

          # Host
          success_01: "Traded with {{deal}} {{unit_01}}",
          bid_01: "Bidded {{bid}} {{unit_01}}",
          yet_01: "Not bid yet",
          none_01: "Not joined",
          registrant_num_01: "Participants: {{number}} players."
        }
      },
      ja: %{
        translations: %{
          variables: %{
            seller_01: "売り手",
            buyer_01: "買い手",
            budget_01: "留保価格",
            cost_01: "費用",
            goods_01: "仮想財",
            send_01: "送信",
            unit_01: "円",
            price_01: "価格",
            profit_01: "利益",
            selling_price_01: "売値",
            buying_price_01: "買値",
            selling_num_01: "売り数量",
            buying_num_01: "買い数量"
          },
          host: %{
            title_01: "ダブルオークション",
            stepper: %{
              instruction_page_01: "説明",
              experiment_page_01: "実験",
              result_page_01: "結果",
              back_01: "戻る",
              next_01: "次へ"
            },
            setting: %{
              title_01: "設定",
              send_01: "送信",
              cancel_01: "キャンセル",
              language: %{
                title_01: "言語設定",
                en_01: "英語",
                ja_01: "日本語"
              },
              game: %{
                title_01: "ゲーム設定",
                header_item_01: "項目",
                header_info_01: "情報",
                setting_01: "設定",
                ex_type_setting_01: "モード設定",
                simple_01: "簡易モード",
                real_01: "厳密モード",
                custom_01: "カスタムモード",
                price_base_01: "お金の初期値",
                price_inc_01: "お金の増分",
                price_max_01: "お金の上限",
                price_min_01: "お金の下限",
                apply_01: "適用",
                edit_01: "編集"
              },
              custom: %{
                title_01: "カスタムモード設定",
                error_price_01: "入力された価格が不正です",
                error_price_zero_01: "価格を1つ以上設定してください",
                error_price_number_01: "参加者の人数以上の価格を設定してください",
                demand_01: "需要",
                supply_01: "供給",
                players_01: "現在の参加者数:{{players_num}}人, 設定された人数:{{setting_num}}人"
              }
            },
            guest_table: %{
              id_01: "Id",
              role_01: "役割",
              state_01: "状態",
              status: %{
                instruction_01: "説明",
                result_01: "結果",
                finished_01: "終了",
                playing_01: "実験中"
              }
            }
          },
          guest: %{
            instruction: %{
              instruction_title_01: "ゲームの説明",
              next_01: "next",
              back_01: "back",
              instructions_01: [
                "<p>これからある{{goods_01}}の取引を行います。</p><p>参加者は{{seller_01}}と{{buyer_01}}に分かれます。</p><p>あなたは、なるべく多くの{{profit_01}}が出るように取引してください。</p>",
                "<p>{{seller_01}}は、なるべく高い{{price_01}}で{{goods_01}}を販売し、多くの{{profit_01}}を上げてください。</p><p>また、損失が出ないように{{cost_01}}以上の{{price_01}}を入力してください。</p>",
                "<p>{{buyer_01}}は、なるべく低い{{price_01}}で{{goods_01}}を購入し、多くの{{profit_01}}を上げてください。</p><p>また、損失が出ないように{{budget_01}}以下の{{price_01}}を入力してください。</p>",
                "<p>提案{{price_01}}は正の整数で入力しなければなりません。また、提案{{price_01}}は取引が成立するまで何度でも変更することができます。</p>",
                "<p>質問がある場合は、いま受け付けます。</p><p>実験中に質問をしたり確認をすることは禁止します。</p>"
              ]
            },
            experiment: %{
              your_buyer_01:
                "<p>あなたは{{buyer_01}}です。</p><p>損失が出ないように{{budget_01}}({{money}}{{unit_01}})以下の{{price_01}}を入力してください。</p>",
              your_buyer_n_01:
                "あなたは{{buyer_01}}です。損失が出ないように{{budget_01}}({{money}}{{unit_01}})以下の{{price_01}}を入力してください。",
              your_seller_01:
                "<p>あなたは{{seller_01}}です。</p><p>損失が出ないように{{cost_01}}({{money}}{{unit_01}})以上の{{price_01}}を入力してください。</p>",
              your_seller_n_01:
                "あなたは{{seller_01}}です。損失が出ないように{{cost_01}}({{money}}{{unit_01}})以上の{{price_01}}を入力してください。",
              donot_join_01: "<p>あなたは現在進行中のダブルオークションには参加していません。</p>",
              success_text_01:
                "<p>取引成立</p><p>{{deal}}{{unit_01}}で取引が成立しました。</p><p>（あなたの提案: {{bid}}{{unit_01}}）</p><p>{{profit_01}}は{{benefit}}{{unit_01}}です。</p><p>実験終了までしばらくお待ちください。</p>",
              buyer_suggest_01: "あなたは{{buyer_01}}として{{bid}}{{unit_01}}で提案中です。",
              seller_suggest_01: "あなたは{{seller_01}}として{{bid}}{{unit_01}}で提案中です。",
              error_pos_01: "正の整数を入力してください",
              error_low_01: "{{budget_01}}{{unit_01}}以下の{{price_01}}で提案して下さい",
              error_up_01: "{{cost_01}}{{unit_01}}以上の{{price_01}}で提案して下さい",
              error_non_01: "値を入力してください",
              suggest_price_01: "提案金額",
              your_suggestion_01: "あなたは{{bid}}{{unit_01}}で提案しました。",
              check_bid_01: "入札します．よろしいですか？",
              check_bid_cancel_01: "入札を取り下げます．よろしいですか？",
              bid_cancel_01: "入札を取り下げる",
              bid_01: "入札する",
              cancel_01: "キャンセル",
              ok_01: "OK",
              close_01: "閉じる"
            },
            result: %{
              table_title_01:
                "{{buyer_01}}：{{buyer_num}}人、{{seller_01}}：{{seller_num}}人、成立済み：{{deals_num}}件\n{{buyer_01}}最高値：{{max_value}}{{unit_01}}、{{seller_01}}最安値：{{min_value}}{{unit_01}}",
              success_price_01: "成立{{price_01}}",
              deals_num_title_01: "成立順",
              deals_num_01: "{{i}}組目",
              graph_01: "結果グラフ",
              title_transition_01: "成立{{price_01}}の推移",
              title_curve_01: "理論的な需要・供給曲線",
              title_surplus_01: "消費者余剰、生産者余剰、総余剰",
              title_rank_01: "総合順位",
              title_bids_table_01: "成立価格表",
              surplus_uni_01:
                "理論的な値：消費者余剰{{consumer_surplus}}{{unit_01}}, 生産者余剰{{producer_surplus}}{{unit_01}}, 総余剰{{total_surplus}}{{unit_01}}",
              surplus_some_01:
                "理論的な値：消費者余剰{{consumer_surplus_min}}〜{{consumer_surplus_max}}{{unit_01}}, 生産者余剰{{producer_surplus_min}}〜{{producer_surplus_max}}{{unit_01}}, 総余剰{{total_surplus_ideal}}{{unit_01}}",
              surplus_01:
                "実験データ：消費者余剰{{consumer_surplus}}{{unit_01}}, 生産者余剰{{producer_surplus}}{{unit_01}}, 総余剰{{total_surplus}}{{unit_01}}",
              chart: %{
                surplus_01:
                  "消費者余剰：{{consumer_surplus}}{{unit_01}}, 生産者余剰：{{producer_surplus}}{{unit_01}}, 総余剰：{{total_surplus}}{{unit_01}}",
                chart_title_01: "理論的な需要・供給曲線",
                ideal_price_01: "理論的な均衡{{price_01}}({{min}}{{unit_01}}～{{max}}{{unit_01}})",
                number_01: "数量",
                ideal_number_01: "理論的な均衡取引数量({{number}})",
                demand_01: "需要",
                supply_01: "供給",
                price_change_01: "成立{{price_01}}の推移",
                success_order_01: "成立順"
              },
              rank: %{
                rank_01: "総合順位",
                rank_seller_01: "順位(売り手)",
                rank_buyer_01: "順位(買い手)",
                key_01: "id",
                profit_01: "利益"
              },
              agreed_price: %{
                board: "板",
                table: "表"
              }
            }
          },

          # Host
          success_01: "{{deal}}{{unit_01}}で成立",
          bid_01: "{{bid}}{{unit_01}}を入札",
          yet_01: "未入札",
          none_01: "不参加",
          registrant_num_01: "登録者{{number}}人"
        }
      }
    }
  end
end
