defmodule Oxpt.DoubleAuction.Host do
  @moduledoc """
  Documentation for Oxpt.DoubleAuction.Host
  """

  use Cizen.Automaton
  defstruct [:room_id, :guest_game_id]

  use Cizen.Effects
  use Cizen.Effectful
  alias Oxpt.Persistence

  alias Cizen.Filter
  alias Cizen.Saga
  alias Oxpt.Game.JoinGuest
  alias Oxpt.Player.{Input, Request}
  alias Cizen.Automaton.Call
  alias Oxpt.DoubleAuction.Host.PlayerSocket

  alias Oxpt.DoubleAuction.Events.{
    UpdateState,
    UpdateStateAll,
    ChangePage,
    ChangeSetting
  }

  alias Oxpt.GetLog

  use Oxpt.Game,
    root_dir: Path.join(__ENV__.file, "../../host") |> Path.expand()

  def player_socket(game_id, %__MODULE__{guest_game_id: guest_game_id}, guest_id) do
    %__MODULE__.PlayerSocket{
      game_id: game_id,
      guest_id: guest_id,
      guest_game_id: guest_game_id
    }
  end

  @impl Oxpt.Game
  def join_guest(join_game) do
    Saga.call(join_game.game_id, join_game)
  end

  @impl Oxpt.Game
  def input(_input) do
    # Currently not used here
    # PlayerSocket.handle_input(input)
  end

  @impl Oxpt.Game
  def request(request) do
    PlayerSocket.handle_request(request)
  end

  @impl Oxpt.Game
  def new(room_id, params) do
    %__MODULE__{room_id: room_id, guest_game_id: params[:game_id]}
  end

  @impl true
  def spawn(%__MODULE__{room_id: room_id, guest_game_id: guest_game_id}) do
    id = Saga.self()
    Persistence.Game.setup(id, room_id)

    perform(%Subscribe{
      event_filter:
        Filter.any([
          Filter.new(fn %Input{game_id: game_id} ->
            game_id == id or game_id == guest_game_id
          end),
          Filter.new(fn %UpdateStateAll{game_id: ^guest_game_id} -> true end),
          Filter.new(fn %UpdateState{game_id: ^guest_game_id} -> true end),
          Filter.new(fn %Request{game_id: game_id} ->
            game_id == id or game_id == guest_game_id
          end),
          Filter.new(fn %GetLog{game_id: ^id} -> true end)
        ])
    })

    initial_state = %{
      guest_game_id: guest_game_id,
      log: []
    }

    {:loop, initial_state}
  end

  @impl true
  def yield({:loop, state}) do
    event = perform(%Receive{})

    state =
      case event do
        %Call{from: from, request: %JoinGuest{game_id: game_id, guest_id: guest_id, game: game}} ->
          GenServer.start_link(__MODULE__.PlayerSocket, player_socket(game_id, game, guest_id))
          Saga.reply(from, :ok)
          state

        %Call{from: from, request: %GetLog{}} ->
          Saga.reply(
            from,
            ["Timestamp\tEvent"] ++ state.log
          )

          state

        %Call{from: from, request: %event{} = request}
        when event in [ChangePage, ChangeSetting] ->
          spawn_link(fn ->
            reply = Saga.call(state.guest_game_id, request)
            Saga.reply(from, reply)
          end)

        event ->
          update_in(state, [:log], fn log ->
            [
              "#{Date.utc_today()}_#{Time.utc_now()}\t#{inspect(event)}"
            ] ++ log
          end)
      end

    {:loop, state}
  end
end
