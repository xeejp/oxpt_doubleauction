defmodule Oxpt.DoubleAuction.Guest.PlayerSocket do
  defstruct [:game_id, :guest_id, :channel_pid]

  use GenServer

  alias Cizen.{Dispatcher, Filter, Saga}
  alias Oxpt.Player.{Input, Output, Request}

  alias Oxpt.DoubleAuction.Events.{
    UpdateState,
    UpdateStateAll,
    FetchState,
    Read,
    Bid,
    BidCancel
  }

  require Filter

  @impl GenServer
  def init(%__MODULE__{game_id: game_id, guest_id: guest_id} = socket) do
    Dispatcher.listen(
      Filter.any([
        Filter.new(fn %UpdateStateAll{game_id: ^game_id} -> true end),
        Filter.new(fn %UpdateState{game_id: ^game_id, guest_id: ^guest_id} ->
          true
        end)
      ])
    )

    Dispatcher.dispatch(%FetchState{
      game_id: game_id,
      guest_id: guest_id
    })

    state = %{
      throttled?: false,
      updated?: false,
      state: %{}
    }

    {:ok, socket |> Map.from_struct() |> Map.merge(state)}
  end

  @impl GenServer
  def handle_info(%event_type{state: next_state}, state)
      when event_type in [UpdateStateAll, UpdateState] do
    state = update_in(state.state, &Map.merge(&1, get_merge_data(next_state, state.guest_id)))
    state = put_in(state.updated?, true)

    {:noreply, state, {:continue, :try_send}}
  end

  @impl GenServer
  def handle_info(:end_of_throttled_interval, state) do
    state = put_in(state.throttled?, false)

    {:noreply, state, {:continue, :try_send}}
  end

  @impl GenServer
  def handle_info({:response, _, _}, state), do: {:noreply, state}

  @impl GenServer
  def handle_continue(:try_send, state) do
    state =
      if state.updated? and not state.throttled? do
        Dispatcher.dispatch(%Output{
          game_id: state.game_id,
          guest_id: state.guest_id,
          event: "update_state",
          payload: %{state: state.state}
        })

        state = put_in(state.throttled?, true)
        state = put_in(state.updated?, false)
        # 30fps
        Process.send_after(self(), :end_of_throttled_interval, 33)
        state
      else
        state
      end

    {:noreply, state}
  end

  def handle_input(%Input{event: "read"} = input) do
    Dispatcher.dispatch(%Read{
      game_id: input.game_id,
      guest_id: input.guest_id
    })
  end

  def handle_request(%Request{event: "bid", payload: bid} = request) do
    Saga.call(request.game_id, %Bid{
      game_id: request.game_id,
      guest_id: request.guest_id,
      bid: bid
    })
  end

  def handle_request(%Request{event: "bid cancel"} = request) do
    Saga.call(request.game_id, %BidCancel{
      game_id: request.game_id,
      guest_id: request.guest_id
    })
  end

  defp get_merge_data(state, guest_id) do
    player = get_in(state, [:players, guest_id]) || %{}

    if get_in(state, [:page]) == "result" do
      state
    else
      state |> Map.drop([:players])
    end
    |> Map.merge(player)
  end
end
