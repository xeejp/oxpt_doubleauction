defmodule Oxpt.DoubleAuction.Guest do
  @moduledoc """
  Documentation for Oxpt.DoubleAuction.Guest
  """

  use Cizen.Automaton
  defstruct [:room_id]

  use Cizen.Effects
  use Cizen.Effectful
  alias Cizen.Filter
  alias Cizen.Saga
  alias Cizen.Automaton.Call
  alias Oxpt.Game.JoinGuest
  alias Oxpt.JoinGame
  alias Oxpt.Persistence
  alias Oxpt.Game
  alias Oxpt.DoubleAuction.Guest.PlayerSocket
  alias Oxpt.Lounge.Host.AddedDummyGuest

  alias Oxpt.DoubleAuction.{Host, Locales}

  alias Oxpt.DoubleAuction.Events.{
    FetchState,
    Read,
    UpdateState,
    UpdateStateAll,
    ChangePage,
    ChangeSetting,
    Bid,
    BidCancel
  }

  use Oxpt.Game,
    root_dir: Path.join(__ENV__.file, "../../guest") |> Path.expand()

  def player_socket(game_id, guest_id) do
    %__MODULE__.PlayerSocket{game_id: game_id, guest_id: guest_id}
  end

  @impl Oxpt.Game
  def join_guest(join_game) do
    Saga.call(join_game.game_id, join_game)
  end

  @impl Oxpt.Game
  def input(input) do
    PlayerSocket.handle_input(input)
  end

  @impl Oxpt.Game
  def request(request) do
    PlayerSocket.handle_request(request)
  end

  @impl Oxpt.Game
  def new(room_id, _params) do
    %__MODULE__{room_id: room_id}
  end

  @impl Oxpt.Game
  def metadata(),
    do: %{
      label: "label_double_auction",
      category: "category_market_mechanism"
    }

  @impl true
  def spawn(%__MODULE__{room_id: room_id}) do
    id = Saga.self()
    Persistence.Game.setup(id, room_id)

    host_game_id =
      unless Application.get_env(:oxpt, :restoring, false) do
        perform(%Start{saga: Host.new(room_id, game_id: id)})
      else
        nil
      end

    perform(%Subscribe{
      event_filter:
        Filter.any([
          Filter.new(fn %FetchState{game_id: ^id} -> true end),
          Filter.new(fn %Read{game_id: ^id} -> true end),
          Filter.new(fn %ChangePage{game_id: ^id} -> true end),
          Filter.new(fn %ChangeSetting{game_id: ^id} -> true end),
          Filter.new(fn %JoinGame{game_id: ^id} -> true end),
          Filter.new(fn %AddedDummyGuest{game_id: ^id} -> true end)
        ])
    })

    initial_state = %{
      host_game_id: host_game_id,
      host_guest_id: nil,
      guest_game_id: id,
      guest_id: nil,
      players: %{},
      dummy_guest_list: [],
      locales: Locales.get(),
      page: "instruction",
      doubleauction_results: [],
      counter: 0,
      ex_type: "simple",
      buyer_bids: [],
      seller_bids: [],
      deals: [],
      highest_bid: nil,
      lowest_bid: nil,
      price_base: 100,
      price_inc: 100,
      price_max: 20,
      price_min: 10,
      buyer_price: [1, 4, 2, 3],
      seller_price: [2, 5, 4, 3]
    }

    {:loop, initial_state}
  end

  @impl true
  def yield({:loop, state}) do
    id = Saga.self()
    event = perform(%Receive{})

    new_state = handle_event_body(id, event, state)
    # IO.inspect Map.drop(new_state, [:locales])
    {:loop, new_state}
  end

  def handle_event_body(
        id,
        %Call{from: from, request: %JoinGuest{guest_id: guest_id}},
        state
      ) do
    GenServer.start_link(__MODULE__.PlayerSocket, player_socket(id, guest_id))
    Saga.reply(from, :ok)

    state
  end

  def handle_event_body(_id, %JoinGame{guest_id: guest_id, host: true}, state) do
    perform(%Dispatch{
      body: %JoinGame{game_id: state.host_game_id, guest_id: guest_id, host: true}
    })

    %{state | host_guest_id: guest_id}
  end

  def handle_event_body(id, %JoinGame{guest_id: guest_id}, state) do
    player = new_player(state, guest_id)
    new_state = put_in(state, [:players, guest_id], player)

    case state.page do
      "instruction" ->
        perform(%Dispatch{
          body: %UpdateState{
            game_id: id,
            guest_id: guest_id,
            state: %{
              players: get_in(new_state, [:players])
            }
          }
        })

        # ignored when state.host_guest_id is nil.
        perform(%Dispatch{
          body: %UpdateState{
            game_id: id,
            guest_id: state.host_guest_id,
            state: %{
              players: get_in(new_state, [:players])
            }
          }
        })

      _ ->
        perform(%Dispatch{
          body: %UpdateStateAll{
            game_id: id,
            state: %{
              players: get_in(new_state, [:players])
            }
          }
        })
    end

    new_state
  end

  def handle_event_body(id, %FetchState{guest_id: guest_id}, state) do
    perform(%Dispatch{
      body: %UpdateState{
        game_id: id,
        guest_id: guest_id,
        state: state
      }
    })

    state
  end

  def handle_event_body(id, %AddedDummyGuest{dummy_guest_list: dummy_guest_list}, state) do
    new_state = put_in(state, [:dummy_guest_list], dummy_guest_list)

    perform(%Dispatch{
      body: %UpdateStateAll{
        game_id: id,
        state: %{
          dummy_guest_list: get_in(new_state, [:dummy_guest_list])
        }
      }
    })

    new_state
  end

  def handle_event_body(id, %Read{guest_id: guest_id}, state) do
    new_state = put_in(state, [:players, guest_id, :read], true)

    perform(%Dispatch{
      body: %UpdateState{
        game_id: id,
        guest_id: guest_id,
        state: %{
          players: get_in(new_state, [:players])
        }
      }
    })

    # ignored when state.host_guest_id is nil
    perform(%Dispatch{
      body: %UpdateState{
        game_id: id,
        guest_id: state.host_guest_id,
        state: %{
          players: get_in(new_state, [:players])
        }
      }
    })

    new_state
  end

  def handle_event_body(id, %Call{from: from, request: %ChangePage{page: page}}, state) do
    new_state =
      state
      |> Map.put(:page, page)

    new_state =
      if state.page == "instruction" && page == "experiment" do
        reset(new_state)
      else
        new_state
      end
      |> set_status(page)

    perform(%Dispatch{
      body: %UpdateStateAll{
        game_id: id,
        state: new_state
      }
    })

    Saga.reply(from, :ok)

    new_state
  end

  def handle_event_body(id, %Call{from: from, request: %ChangeSetting{payload: payload}}, state) do
    new_state =
      state
      |> Map.merge(%{
        ex_type: payload["ex_type"],
        price_base: payload["price_base"],
        price_inc: payload["price_inc"],
        price_max: payload["price_max"],
        price_min: payload["price_min"],
        buyer_price: payload["buyer_price"],
        seller_price: payload["seller_price"]
      })
      |> update_locales(payload["locales_temp"])

    perform(%Dispatch{
      body: %UpdateStateAll{
        game_id: id,
        state: new_state
      }
    })

    Saga.reply(from, :ok)

    new_state
  end

  def handle_event_body(
        id,
        %Call{from: from, request: %Bid{guest_id: guest_id, bid: bid}},
        state
      ) do
    # buyer_bids, seller_bids, deals, highest_bid, lowest_bid, players, counterを更新
    if bid > 0 do
      player = get_in(state, [:players, guest_id])

      new_state =
        case player do
          %{role: "seller", bid: previous_bid, money: money, dealt: false}
          when not is_nil(money) and bid >= money ->
            state =
              remove_first(
                state,
                guest_id,
                previous_bid,
                :lowest_bid,
                :seller_bids,
                &set_lowest_bid/1
              )
              |> update_bid(guest_id, bid)

            if not is_nil(state.highest_bid) and bid <= state.highest_bid.bid do
              deal(
                state,
                :highest_bid,
                :lowest_bid,
                :buyer_bids,
                :seller_bids,
                guest_id,
                state.highest_bid.bid,
                &set_highest_bid/1
              )
            else
              bid(state, :lowest_bid, :seller_bids, guest_id, bid, fn most_bid, bid ->
                bid < most_bid
              end)
            end

          %{role: "buyer", bid: previous_bid, money: money, dealt: false}
          when not is_nil(money) and bid <= money ->
            state =
              remove_first(
                state,
                guest_id,
                previous_bid,
                :highest_bid,
                :buyer_bids,
                &set_highest_bid/1
              )
              |> update_bid(guest_id, bid)

            if not is_nil(state.lowest_bid) and bid >= state.lowest_bid.bid do
              deal(
                state,
                :lowest_bid,
                :highest_bid,
                :seller_bids,
                :buyer_bids,
                guest_id,
                state.lowest_bid.bid,
                &set_lowest_bid/1
              )
            else
              bid(state, :highest_bid, :buyer_bids, guest_id, bid, fn most_bid, bid ->
                bid > most_bid
              end)
            end

          _ ->
            state
        end

      perform(%Dispatch{
        body: %UpdateStateAll{
          game_id: id,
          state: %{
            buyer_bids: get_in(new_state, [:buyer_bids]),
            players: get_in(new_state, [:players]),
            seller_bids: get_in(new_state, [:seller_bids]),
            deals: get_in(new_state, [:deals]),
            highest_bid: get_in(new_state, [:highest_bid]),
            lowest_bid: get_in(new_state, [:lowest_bid]),
            counter: get_in(new_state, [:counter])
          }
        }
      })

      Saga.reply(from, :ok)

      new_state
    else
      Saga.reply(from, :ok)

      state
    end
  end

  def handle_event_body(id, %Call{from: from, request: %BidCancel{guest_id: guest_id}}, state) do
    # buyer_bids, seller_bids, highest_bid, lowest_bid, playersを更新
    player = get_in(state, [:players, guest_id])

    new_state =
      case player do
        %{role: "seller", bid: previous_bid, bidded: true, dealt: false} ->
          remove_first(
            state,
            guest_id,
            previous_bid,
            :lowest_bid,
            :seller_bids,
            &set_lowest_bid/1
          )
          |> cancel_bid(guest_id, :lowest_bid, :seller_bids)

        %{role: "buyer", bid: previous_bid, bidded: true, dealt: false} ->
          remove_first(
            state,
            guest_id,
            previous_bid,
            :highest_bid,
            :buyer_bids,
            &set_highest_bid/1
          )
          |> cancel_bid(guest_id, :highest_bid, :buyer_bids)

        _ ->
          state
      end

    perform(%Dispatch{
      body: %UpdateStateAll{
        game_id: id,
        state: %{
          buyer_bids: get_in(new_state, [:buyer_bids]),
          players: get_in(new_state, [:players]),
          seller_bids: get_in(new_state, [:seller_bids]),
          highest_bid: get_in(new_state, [:highest_bid]),
          lowest_bid: get_in(new_state, [:lowest_bid])
        }
      }
    })

    Saga.reply(from, :ok)
    new_state
  end

  def update_bid(state, guest_id, bid) do
    update_in(state, [:players, guest_id], fn player ->
      %{player | bidded: true, bid: bid}
    end)
  end

  def cancel_bid(state, guest_id, bid_key, key) do
    player_list = state[:players]
    bids = state[key]

    sorted_bids =
      if player_list[guest_id].role == "seller" do
        Enum.sort(bids, fn x, y -> x.bid < y.bid end)
      else
        Enum.sort(bids, fn x, y -> x.bid > y.bid end)
      end

    most_bid = Enum.at(sorted_bids, 0)

    update_in(state, [:players, guest_id], fn player ->
      %{player | bidded: false, bid: nil}
    end)
    |> put_in([bid_key], most_bid)
  end

  def remove_first(state, guest_id, previous_bid, bid_key, key, set) do
    if previous_bid != nil do
      new_state = %{
        state
        | key =>
            Enum.filter(state[key], fn map ->
              map.guest_id != guest_id
            end)
      }

      if not is_nil(state[bid_key]) and state[bid_key].guest_id == guest_id do
        set.(new_state)
      else
        new_state
      end
    else
      state
    end
  end

  def bid(state, bid_key, key, guest_id, bid, _func) do
    player_list = state[:players]
    new_bid = new_bid(state.counter, guest_id, bid)
    bids = [new_bid | state[key]]

    sorted_bids =
      if player_list[guest_id].role == "seller" do
        Enum.sort(bids, fn x, y -> x.bid < y.bid end)
      else
        Enum.sort(bids, fn x, y -> x.bid > y.bid end)
      end

    most_bid = Enum.at(sorted_bids, 0)

    state
    |> put_in([key], bids)
    |> put_in([bid_key], most_bid)
    |> update_in([:counter], fn x -> x + 1 end)

    # |> update_in([:doubleauction_results], fn results -> results ++ [new_result(guest_id, bid, "bid", nil, Timex.format!(Timex.now(), "{ISO:Extended}"))] end)
  end

  def new_bid(bid_id, guest_id, bid) do
    %{
      bid: bid,
      bid_id: bid_id,
      guest_id: guest_id
    }
  end

  def deal(state, bid_key, bid_key_another, partner_key, key, guest_id, bid, set) do
    now = Timex.format!(Timex.now(), "{ISO:Extended}")
    guest_id2 = state[bid_key].guest_id
    deals = [new_deal(state.counter, bid, guest_id, guest_id2, now) | state.deals]
    bids = List.delete(state[partner_key], state[bid_key])
    bids_another = state[key]
    player_list = state[:players]

    sorted_bids =
      if player_list[guest_id].role == "seller" do
        Enum.sort(bids, fn x, y -> x.bid > y.bid end)
      else
        Enum.sort(bids, fn x, y -> x.bid < y.bid end)
      end

    sorted_bids_another =
      if player_list[guest_id].role == "seller" do
        Enum.sort(bids_another, fn x, y -> x.bid < y.bid end)
      else
        Enum.sort(bids_another, fn x, y -> x.bid > y.bid end)
      end

    most_bid = Enum.at(sorted_bids, 0)
    most_bid_another = Enum.at(sorted_bids_another, 0)

    new_state =
      %{state | :deals => deals, partner_key => bids}
      |> dealt(guest_id, guest_id2, state[bid_key].bid)
      |> Map.update!(:counter, fn x -> x + 1 end)
      |> put_in([bid_key], most_bid)
      |> put_in([bid_key_another], most_bid_another)

    new_state = set.(new_state)

    new_state
    |> Map.put(
      :doubleauction_results,
      state.doubleauction_results ++ [new_result(guest_id, bid, "deal", guest_id2, now)]
    )
  end

  def new_deal(deal_id, bid, guest_id, guest_id2, now) do
    %{
      deal_id: deal_id,
      deal: bid,
      time: now,
      guest_id: guest_id,
      guest_id2: guest_id2
    }
  end

  def dealt(state, guest_id, guest_id2, money) do
    player_1 = get_in(state, [:players, guest_id])
    player_2 = get_in(state, [:players, guest_id2])

    new_state =
      case player_1 do
        %{role: "seller"} ->
          state
          |> put_in([:players, guest_id, :profit], money - player_1.money)

        %{role: "buyer"} ->
          state
          |> put_in([:players, guest_id, :profit], player_1.money - money)
      end

    new_state =
      case player_2 do
        %{role: "seller"} ->
          new_state
          |> put_in([:players, guest_id2, :profit], money - player_2.money)

        %{role: "buyer"} ->
          new_state
          |> put_in([:players, guest_id2, :profit], player_2.money - money)
      end

    new_state
    |> update_in([:players, guest_id], fn player ->
      %{player | bidded: false, dealt: true, finished: true, deal: money, status: "dealt"}
    end)
    |> update_in([:players, guest_id2], fn player ->
      %{player | bidded: false, dealt: true, finished: true, deal: money, status: "dealt"}
    end)
  end

  def new_result(guest_id1, bid, status, guest_id2, now) do
    %{guest_id1: guest_id1, price: bid, status: status, guest_id2: guest_id2, time: now}
  end

  def set_highest_bid(%{buyer_bids: []} = state) do
    %{state | highest_bid: nil}
  end

  def set_highest_bid(%{buyer_bids: bids} = state) do
    %{state | highest_bid: Enum.max_by(bids, & &1.bid)}
  end

  def set_lowest_bid(%{seller_bids: []} = state) do
    %{state | lowest_bid: nil}
  end

  def set_lowest_bid(%{seller_bids: bids} = state) do
    %{state | lowest_bid: Enum.min_by(bids, & &1.bid)}
  end

  def reset(state) do
    new_state =
      match(state)
      |> Map.merge(%{
        buyer_bids: [],
        seller_bids: [],
        deals: [],
        highest_bid: nil,
        lowest_bid: nil
      })

    new_state
  end

  def match(state) do
    if state.ex_type != "custom" do
      reduce_state =
        Enum.reduce(
          Enum.shuffle(Map.keys(state.players)),
          %{acc_state: state, acc: 0},
          fn guest_id, %{acc_state: acc_state, acc: acc} ->
            new_player =
              new_player(state, guest_id)
              |> Map.merge(
                if rem(acc, 2) == 1 do
                  %{
                    role: "buyer",
                    money:
                      case state.ex_type do
                        "simple" ->
                          acc * state.price_inc + state.price_base

                        "real" ->
                          :rand.uniform(state.price_max - state.price_min + 1) + state.price_min -
                            1
                      end,
                    joined: true
                  }
                else
                  %{
                    role: "seller",
                    money:
                      case state.ex_type do
                        "simple" ->
                          acc * state.price_inc + state.price_base

                        "real" ->
                          :rand.uniform(state.price_max - state.price_min + 1) + state.price_min -
                            1
                      end,
                    joined: true
                  }
                end
              )

            %{
              acc_state: put_in(acc_state, [:players, guest_id], new_player),
              acc: acc + 1
            }
          end
        )

      reduce_state.acc_state
    else
      reduced = custom_price_set(state)

      reduced.acc_state
    end
  end

  def custom_price_set(state) do
    player_num = length(Map.keys(state.players))
    setting_num = length(state.buyer_price ++ state.seller_price)

    [buyer_price, seller_price] =
      if player_num > setting_num do
        sorted_buyer = Enum.sort(state.buyer_price, &(&1 >= &2))
        sorted_seller = Enum.sort(state.seller_price, &(&1 <= &2))

        max_length = Enum.max_by([sorted_buyer, sorted_seller], &length(&1)) |> length

        reduced =
          1..max_length
          |> Enum.reduce(
            %{
              equi_price_min: hd(sorted_seller),
              equi_price_max: hd(sorted_buyer),
              acc_buyer: sorted_buyer,
              acc_seller: sorted_seller
            },
            fn _,
               %{
                 equi_price_min: equi_price_min,
                 equi_price_max: equi_price_max,
                 acc_buyer: acc_buyer,
                 acc_seller: acc_seller
               } ->
              equi_price_max_temp =
                if Enum.empty?(acc_buyer), do: equi_price_max, else: hd(acc_buyer)

              equi_price_min_temp =
                if Enum.empty?(acc_seller), do: equi_price_min, else: hd(acc_seller)

              [equi_price_max_temp, equi_price_min_temp] =
                if equi_price_max_temp < equi_price_min_temp do
                  [equi_price_max, equi_price_min]
                else
                  [equi_price_max_temp, equi_price_min_temp]
                end

              %{
                equi_price_min: equi_price_min_temp,
                equi_price_max: equi_price_max_temp,
                acc_buyer: if(Enum.empty?(acc_buyer), do: acc_buyer, else: tl(acc_buyer)),
                acc_seller: if(Enum.empty?(acc_seller), do: acc_seller, else: tl(acc_seller))
              }
            end
          )

        equi_price_min = reduced.equi_price_min
        equi_price_max = reduced.equi_price_max

        redeuced =
          1..(player_num - setting_num)
          |> Enum.reduce(%{acc_buyer: sorted_buyer, acc_seller: sorted_seller, acc: 0}, fn _,
                                                                                           %{
                                                                                             acc_buyer:
                                                                                               acc_buyer,
                                                                                             acc_seller:
                                                                                               acc_seller,
                                                                                             acc:
                                                                                               acc
                                                                                           } ->
            if rem(acc, 2) == 1 do
              price =
                if equi_price_min <= 1 do
                  equi_price_min
                else
                  :rand.uniform(equi_price_min - 1)
                end

              %{
                acc_buyer: [price | acc_buyer],
                acc_seller: acc_seller,
                acc: acc + 1
              }
            else
              max_price = Enum.max(state.buyer_price ++ state.seller_price)

              price =
                if equi_price_max == max_price do
                  equi_price_max
                else
                  :rand.uniform(max_price - equi_price_max) + equi_price_max
                end

              %{
                acc_buyer: acc_buyer,
                acc_seller: [price | acc_seller],
                acc: acc + 1
              }
            end
          end)

        [redeuced.acc_buyer, redeuced.acc_seller]
      else
        [state.buyer_price, state.seller_price]
      end

    state.players
    |> Map.keys()
    |> Enum.shuffle()
    |> Enum.reduce(
      %{
        acc_state: state,
        acc_buyer: buyer_price,
        acc_seller: seller_price,
        acc: 0
      },
      fn guest_id,
         %{acc_state: acc_state, acc_buyer: acc_buyer, acc_seller: acc_seller, acc: acc} ->
        [new_player, next_buyer, next_seller] =
          if not Enum.empty?(acc_buyer) && (rem(acc, 2) == 1 || Enum.empty?(acc_seller)) do
            [price | next_buyer] = acc_buyer

            [
              %{
                role: "buyer",
                money: price,
                joined: true
              },
              next_buyer,
              acc_seller
            ]
          else
            [price | next_seller] = acc_seller

            [
              %{
                role: "seller",
                money: price,
                joined: true
              },
              acc_buyer,
              next_seller
            ]
          end

        %{
          acc_state:
            put_in(
              acc_state,
              [:players, guest_id],
              new_player(state, guest_id) |> Map.merge(new_player)
            ),
          acc_buyer: next_buyer,
          acc_seller: next_seller,
          acc: acc + 1
        }
      end
    )
  end

  def set_status(state, page) do
    new_players =
      Enum.reduce(Map.keys(state.players), state.players, fn key, acc_players ->
        acc_players
        |> update_in([key], fn player ->
          player
          |> Map.merge(%{
            status:
              case page do
                "experiment" -> "dealing"
                _ -> nil
              end
          })
        end)
      end)

    state
    |> Map.merge(%{
      players: new_players
    })
  end

  def update_locales(state, locales_temp) do
    # localeを変更
    Enum.reduce(Map.keys(locales_temp), state, fn key, acc_state ->
      lang_temp = locales_temp[key]

      update_in(
        acc_state,
        [:locales, String.to_existing_atom(key), :translations],
        fn translations ->
          Enum.reduce(Map.keys(lang_temp), translations, fn trans_key, acc_trans ->
            put_in(
              acc_trans,
              [:variables, String.to_existing_atom(trans_key)],
              lang_temp[trans_key]
            )
          end)
        end
      )
    end)
  end

  def new_player(state, guest_id) do
    %{
      guest_id: guest_id,
      role: nil,
      money: nil,
      profit: 0,
      bidded: false,
      bid: nil,
      dealt: false,
      deal: nil,
      status: nil,
      read: false,
      finished: false,
      joined: state.page != "experiment" && state.page != "result"
    }
  end
end
