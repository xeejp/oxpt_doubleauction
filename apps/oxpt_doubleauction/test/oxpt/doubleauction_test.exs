defmodule Oxpt.DoubleAuctionTest do
  use Oxpt.CizenCase

  alias Oxpt.DoubleAuction.Events.{
    # Read,
    # ChangePage,
    Bid
    # BidCancel
  }

  alias Oxpt.DoubleAuction.Guest

  doctest Oxpt.DoubleAuction.Guest
  doctest Oxpt.DoubleAuction.Host

  setup do
    initial_state = %{
      buyer_bids: [],
      counter: 0,
      deals: [],
      doubleauction_results: [],
      ex_type: "simple",
      guest_game_id: "guest_game_id",
      highest_bid: nil,
      host_game_id: "host_game_id",
      host_guest_id: "host_guest_id",
      lowest_bid: nil,
      page: "experiment",
      buyer_price: [1, 4, 2, 3],
      seller_price: [2, 5, 4, 3],
      players: %{
        "s1" => %{
          bid: nil,
          bidded: false,
          deal: nil,
          dealt: false,
          finished: false,
          joined: true,
          money: 700,
          profit: 0,
          read: false,
          role: "seller",
          status: "dealing"
        },
        "s2" => %{
          bid: nil,
          bidded: false,
          deal: nil,
          dealt: false,
          finished: false,
          joined: true,
          money: 300,
          profit: 0,
          read: false,
          role: "seller",
          status: "dealing"
        },
        "s3" => %{
          bid: nil,
          bidded: false,
          deal: nil,
          dealt: false,
          finished: false,
          joined: true,
          money: 100,
          profit: 0,
          read: false,
          role: "seller",
          status: "dealing"
        },
        "s4" => %{
          bid: nil,
          bidded: false,
          deal: nil,
          dealt: false,
          finished: false,
          joined: true,
          money: 500,
          profit: 0,
          read: false,
          role: "seller",
          status: "dealing"
        },
        "b1" => %{
          bid: nil,
          bidded: false,
          deal: nil,
          dealt: false,
          finished: false,
          joined: true,
          money: 200,
          profit: 0,
          read: false,
          role: "buyer",
          status: "dealing"
        },
        "b2" => %{
          bid: nil,
          bidded: false,
          deal: nil,
          dealt: false,
          finished: false,
          joined: true,
          money: 600,
          profit: 0,
          read: false,
          role: "buyer",
          status: "dealing"
        },
        "b3" => %{
          bid: nil,
          bidded: false,
          deal: nil,
          dealt: false,
          finished: false,
          joined: true,
          money: 800,
          profit: 0,
          read: false,
          role: "buyer",
          status: "dealing"
        },
        "b4" => %{
          bid: nil,
          bidded: false,
          deal: nil,
          dealt: false,
          finished: false,
          joined: true,
          money: 400,
          profit: 0,
          read: false,
          role: "buyer",
          status: "dealing"
        }
      },
      price_base: 100,
      price_inc: 100,
      price_max: 20,
      price_min: 10,
      seller_bids: []
    }

    {:ok, initial_state: initial_state}
  end

  test "売り手が入力したらseller_bidsとlowest_bidを更新", %{initial_state: initial_state} do
    state = initial_state
    expected = state

    assert_handle(fn id ->
      expected1 = %{
        expected
        | seller_bids: [%{guest_id: "s1", bid: 800, bid_id: 0}],
          lowest_bid: %{guest_id: "s1", bid: 800, bid_id: 0}
      }

      state1 = Guest.handle_event_body(id, %Bid{game_id: id, guest_id: "s1", bid: 800}, state)
      assert dropper(expected1) == dropper(state1)

      expected2 = %{
        expected1
        | seller_bids: [
            %{guest_id: "s2", bid: 350, bid_id: 1},
            %{guest_id: "s1", bid: 800, bid_id: 0}
          ],
          lowest_bid: %{guest_id: "s2", bid: 350, bid_id: 1}
      }

      state2 = Guest.handle_event_body(id, %Bid{game_id: id, guest_id: "s2", bid: 350}, state1)
      assert dropper(expected2) == dropper(state2)

      expected3 = %{
        expected2
        | seller_bids: [
            %{guest_id: "s3", bid: 100, bid_id: 2},
            %{guest_id: "s2", bid: 350, bid_id: 1},
            %{guest_id: "s1", bid: 800, bid_id: 0}
          ],
          lowest_bid: %{guest_id: "s3", bid: 100, bid_id: 2}
      }

      state3 = Guest.handle_event_body(id, %Bid{game_id: id, guest_id: "s3", bid: 100}, state2)
      assert dropper(expected3) == dropper(state3)

      expected4 = %{
        expected3
        | seller_bids: [
            %{guest_id: "s4", bid: 1000, bid_id: 3},
            %{guest_id: "s3", bid: 100, bid_id: 2},
            %{guest_id: "s2", bid: 350, bid_id: 1},
            %{guest_id: "s1", bid: 800, bid_id: 0}
          ]
      }

      state4 = Guest.handle_event_body(id, %Bid{game_id: id, guest_id: "s4", bid: 1000}, state3)
      assert dropper(expected4) == dropper(state4)

      expected5 = %{
        expected4
        | seller_bids: [
            %{guest_id: "s3", bid: 400, bid_id: 4},
            %{guest_id: "s4", bid: 1000, bid_id: 3},
            %{guest_id: "s2", bid: 350, bid_id: 1},
            %{guest_id: "s1", bid: 800, bid_id: 0}
          ],
          lowest_bid: %{guest_id: "s2", bid: 350, bid_id: 1}
      }

      state5 = Guest.handle_event_body(id, %Bid{game_id: id, guest_id: "s3", bid: 400}, state4)
      assert dropper(expected5) == dropper(state5)
    end)
  end

  test "買い手が入力したらbuyer_bidsとhighest_bidを更新", %{initial_state: initial_state} do
    state = initial_state
    expected = state

    assert_handle(fn id ->
      expected1 = %{
        expected
        | buyer_bids: [%{guest_id: "b1", bid: 150, bid_id: 0}],
          highest_bid: %{guest_id: "b1", bid: 150, bid_id: 0}
      }

      state1 = Guest.handle_event_body(id, %Bid{game_id: id, guest_id: "b1", bid: 150}, state)
      assert dropper(expected1) == dropper(state1)

      expected2 = %{
        expected1
        | buyer_bids: [
            %{guest_id: "b2", bid: 580, bid_id: 1},
            %{guest_id: "b1", bid: 150, bid_id: 0}
          ],
          highest_bid: %{guest_id: "b2", bid: 580, bid_id: 1}
      }

      state2 = Guest.handle_event_body(id, %Bid{game_id: id, guest_id: "b2", bid: 580}, state1)
      assert dropper(expected2) == dropper(state2)

      expected3 = %{
        expected2
        | buyer_bids: [
            %{guest_id: "b3", bid: 700, bid_id: 2},
            %{guest_id: "b2", bid: 580, bid_id: 1},
            %{guest_id: "b1", bid: 150, bid_id: 0}
          ],
          highest_bid: %{guest_id: "b3", bid: 700, bid_id: 2}
      }

      state3 = Guest.handle_event_body(id, %Bid{game_id: id, guest_id: "b3", bid: 700}, state2)
      assert dropper(expected3) == dropper(state3)

      expected4 = %{
        expected3
        | buyer_bids: [
            %{guest_id: "b4", bid: 300, bid_id: 3},
            %{guest_id: "b3", bid: 700, bid_id: 2},
            %{guest_id: "b2", bid: 580, bid_id: 1},
            %{guest_id: "b1", bid: 150, bid_id: 0}
          ]
      }

      state4 = Guest.handle_event_body(id, %Bid{game_id: id, guest_id: "b4", bid: 300}, state3)
      assert dropper(expected4) == dropper(state4)

      expected5 = %{
        expected4
        | buyer_bids: [
            %{guest_id: "b3", bid: 400, bid_id: 4},
            %{guest_id: "b4", bid: 300, bid_id: 3},
            %{guest_id: "b2", bid: 580, bid_id: 1},
            %{guest_id: "b1", bid: 150, bid_id: 0}
          ],
          highest_bid: %{guest_id: "b2", bid: 580, bid_id: 1}
      }

      state5 = Guest.handle_event_body(id, %Bid{game_id: id, guest_id: "b3", bid: 400}, state4)
      assert dropper(expected5) == dropper(state5)
    end)
  end

  test "売値が買値を下回るか等しい売り注文があったら取引成立", %{initial_state: initial_state} do
    state =
      %{
        initial_state
        | counter: 4,
          buyer_bids: [
            %{guest_id: "b4", bid: 300, bid_id: 3},
            %{guest_id: "b3", bid: 700, bid_id: 2},
            %{guest_id: "b2", bid: 580, bid_id: 1},
            %{guest_id: "b1", bid: 150, bid_id: 0}
          ],
          highest_bid: %{guest_id: "b3", bid: 700, bid_id: 2}
      }
      |> update_in([:players], fn players ->
        %{
          players
          | "b1" => %{
              bid: 150,
              bidded: true,
              deal: nil,
              dealt: false,
              finished: false,
              joined: true,
              money: 200,
              profit: 0,
              read: false,
              role: "buyer",
              status: "dealing"
            },
            "b2" => %{
              bid: 580,
              bidded: true,
              deal: nil,
              dealt: false,
              finished: false,
              joined: true,
              money: 600,
              profit: 0,
              read: false,
              role: "buyer",
              status: "dealing"
            },
            "b3" => %{
              bid: 700,
              bidded: true,
              deal: nil,
              dealt: false,
              finished: false,
              joined: true,
              money: 800,
              profit: 0,
              read: false,
              role: "buyer",
              status: "dealing"
            },
            "b4" => %{
              bid: 300,
              bidded: true,
              deal: nil,
              dealt: false,
              finished: false,
              joined: true,
              money: 400,
              profit: 0,
              read: false,
              role: "buyer",
              status: "dealing"
            }
        }
      end)

    expected = state

    assert_handle(fn id ->
      expected1 = %{
        expected
        | seller_bids: [%{guest_id: "s1", bid: 800, bid_id: 4}],
          lowest_bid: %{guest_id: "s1", bid: 800, bid_id: 4}
      }

      state1 = Guest.handle_event_body(id, %Bid{game_id: id, guest_id: "s1", bid: 800}, state)
      assert dropper(expected1) == dropper(state1)

      expected2 = %{
        expected1
        | buyer_bids: [
            %{guest_id: "b4", bid: 300, bid_id: 3},
            %{guest_id: "b2", bid: 580, bid_id: 1},
            %{guest_id: "b1", bid: 150, bid_id: 0}
          ],
          highest_bid: %{guest_id: "b2", bid: 580, bid_id: 1}
      }

      state2 = Guest.handle_event_body(id, %Bid{game_id: id, guest_id: "s2", bid: 350}, state1)
      assert dropper(expected2) == dropper(state2)
    end)
  end

  test "買値が売値を上回るか等しい買い注文があったら取引成立", %{initial_state: initial_state} do
    state =
      %{
        initial_state
        | counter: 4,
          seller_bids: [
            %{guest_id: "s4", bid: 1000, bid_id: 3},
            %{guest_id: "s3", bid: 100, bid_id: 2},
            %{guest_id: "s2", bid: 350, bid_id: 1},
            %{guest_id: "s1", bid: 800, bid_id: 0}
          ],
          lowest_bid: %{guest_id: "s3", bid: 100, bid_id: 2}
      }
      |> update_in([:players], fn players ->
        %{
          players
          | "s1" => %{
              bid: 800,
              bidded: true,
              deal: nil,
              dealt: false,
              finished: false,
              joined: true,
              money: 700,
              profit: 0,
              read: false,
              role: "seller",
              status: "dealing"
            },
            "s2" => %{
              bid: 350,
              bidded: true,
              deal: nil,
              dealt: false,
              finished: false,
              joined: true,
              money: 300,
              profit: 0,
              read: false,
              role: "seller",
              status: "dealing"
            },
            "s3" => %{
              bid: 100,
              bidded: true,
              deal: nil,
              dealt: false,
              finished: false,
              joined: true,
              money: 100,
              profit: 0,
              read: false,
              role: "seller",
              status: "dealing"
            },
            "s4" => %{
              bid: 1000,
              bidded: true,
              deal: nil,
              dealt: false,
              finished: false,
              joined: true,
              money: 500,
              profit: 0,
              read: false,
              role: "seller",
              status: "dealing"
            }
        }
      end)

    expected = state

    assert_handle(fn id ->
      expected1 = %{
        expected
        | buyer_bids: [%{guest_id: "b1", bid: 90, bid_id: 4}],
          highest_bid: %{guest_id: "b1", bid: 90, bid_id: 4}
      }

      state1 = Guest.handle_event_body(id, %Bid{game_id: id, guest_id: "b1", bid: 90}, state)
      assert dropper(expected1) == dropper(state1)

      expected2 = %{
        expected1
        | seller_bids: [
            %{guest_id: "s4", bid: 1000, bid_id: 3},
            %{guest_id: "s2", bid: 350, bid_id: 1},
            %{guest_id: "s1", bid: 800, bid_id: 0}
          ],
          lowest_bid: %{guest_id: "s2", bid: 350, bid_id: 1}
      }

      state2 = Guest.handle_event_body(id, %Bid{game_id: id, guest_id: "b2", bid: 580}, state1)
      assert dropper(expected2) == dropper(state2)
    end)
  end

  test "設定参加者数よりも少ない参加者の場合，配列の前から順番に割り振っていく", %{initial_state: initial_state} do
    state = %{
      initial_state
      | ex_type: "custom",
        buyer_price: [10, 40, 20, 30, 100, 200, 300],
        seller_price: [21, 51, 41, 31, 101, 201, 301]
    }

    %{players: players} = Guest.match(state)

    buyer_price =
      players
      |> Map.keys()
      |> Enum.filter(&(players[&1].role == "buyer"))
      |> Enum.map(&players[&1].money)
      |> Enum.sort()

    seller_price =
      players
      |> Map.keys()
      |> Enum.filter(&(players[&1].role == "seller"))
      |> Enum.map(&players[&1].money)
      |> Enum.sort()

    assert [10, 20, 30, 40] == buyer_price
    assert [21, 31, 41, 51] == seller_price
  end

  def dropper(map) do
    Map.drop(map, [:players])
    |> Map.drop([:doubleauction_results])
    |> Map.drop([:counter])
    |> Map.drop([:deals])
  end
end
