import React, { useReducer, useEffect } from 'react'
import PropTypes from 'prop-types'
import { useStore } from '../actions/hook'
import { useTranslation } from 'react-i18next'

import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'
import HCExporting from 'highcharts/modules/exporting'

import Grid from '@material-ui/core/Grid'

import i18nInstance from '../i18n'

HCExporting(Highcharts)

const CurveChart = ({ buyerBids, sellerBids, equiPriceMax, equiPriceMin, volume }) => {
  const { locales, exType, priceInc, priceMax, priceMin, deals, players } = useStore()

  const [t, i18n] = useTranslation('translations', { i18nInstance })
  const [, forceUpdate] = useReducer(x => x + 1, 0)

  useEffect(() => {
    locales && Object.keys(locales).map(lang => {
      Object.keys(locales[lang]).map(namespace => {
        i18n.addResourceBundle(lang, namespace, locales[lang][namespace])
      })
    })
    forceUpdate()
  }, [locales])

  if (!(locales && exType && priceInc && priceMax && priceMin && deals && players))
    return <></>

  const variablesObject = () => {
    return (typeof t('variables')) === 'object' ? t('variables') : {}
  }

  const maxTemp = Math.max(...buyerBids.concat(sellerBids))
  const minTemp = Math.min(...buyerBids.concat(sellerBids))
  const max = Math.floor((maxTemp + (maxTemp - minTemp) / 10))
  const min = Math.floor(Math.max(0, minTemp - (maxTemp - minTemp) / 10))

  return (
    <>
      <Grid container>
        <Grid item xs={12}>
          <HighchartsReact
            options = {{
              chart: {
                type: 'area',
                animation: false,
                inverted: true
              },
              title: {
                text: t('guest.result.chart.chart_title_01')
              },
              xAxis: {
                title: {
                  text: variablesObject().price_01
                },
                min: min,
                max: max,
                tickInterval: (exType === 'simple') ? priceInc : Math.floor((priceMax - priceMin) / 10),
                reversed: false,
                plotLines: [{
                  color: 'black',
                  dashStyle: 'dot',
                  width: 2,
                  value: Math.floor((equiPriceMin + equiPriceMax) * 0.5),
                  label: {
                    align: 'right',
                    x: -10,
                    text: t('guest.result.chart.ideal_price_01', { ...variablesObject(), min: equiPriceMin, max: equiPriceMax })
                  },
                  zIndex: 99
                }]
              },
              yAxis: {
                title: {
                  text: t('guest.result.chart.number_01')
                },
                min: 0,
                max: Math.max(buyerBids.length, sellerBids.length) + 1,
                tickInterval: 1,
                plotLines: volume > 0 && [{
                  color: 'black',
                  dashStyle: 'dot',
                  width: 2,
                  value: volume,
                  label: {
                    rotation: 0,
                    y: 15,
                    x: 10,
                    text: t('guest.result.chart.ideal_number_01', { number: volume })
                  },
                  zIndex: 99
                }]
              },
              plotOptions: {
                area: {
                  fillOpacity: 0.5,
                  marker: {
                    enabled: false
                  }
                }
              },
              series: [
                {
                  animation: false,
                  name: t('guest.result.chart.demand_01'),
                  step: 'right',
                  data: [[min, buyerBids.length], ...buyerBids.sort((a, b) => a - b).map((money, index) => [money, buyerBids.length - index]), [max, 0]]
                },
                {
                  animation: false,
                  name: t('guest.result.chart.supply_01'),
                  step: 'left',
                  data: [[min, 0], ...sellerBids.sort((a, b) => a - b).map((money, index) => [money, index + 1]), [max, sellerBids.length]]
                }],
              credits: {
                enabled: false
              },
              exporting: {
                buttons: {
                  contextButton: {
                    symbol: 'menu',
                    menuItems: ['downloadPNG']
                  }
                },
                fallbackToExportServer: false
              }
            }}
            highcharts={Highcharts}
          />
        </Grid>
      </Grid>
    </>
  )
}

CurveChart.propTypes = {
  buyerBids: PropTypes.array,
  sellerBids: PropTypes.array,
  equiPriceMax: PropTypes.number,
  equiPriceMin: PropTypes.number,
  volume: PropTypes.number
}

export default CurveChart
