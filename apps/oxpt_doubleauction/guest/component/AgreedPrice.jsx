import React, { useReducer, useEffect } from 'react'
import { useStore } from '../actions/hook'
import { useTranslation } from 'react-i18next'

import { makeStyles } from '@material-ui/core/styles'

import Table from '@material-ui/core/Table'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import TableCell from '@material-ui/core/TableCell'
import TableBody from '@material-ui/core/TableBody'

import clone from 'clone'

import i18nInstance from '../i18n'

const useStyles = makeStyles(theme => ({
  bidsTable: {
    padding: theme.spacing(1)
  }
}))

export default () => {
  const classes = useStyles()
  const { locales, buyerBids, sellerBids, deals } = useStore()

  const [t, i18n] = useTranslation('translations', { i18nInstance })
  const [, forceUpdate] = useReducer(x => x + 1, 0)

  useEffect(() => {
    locales && Object.keys(locales).map(lang => {
      Object.keys(locales[lang]).map(namespace => {
        i18n.addResourceBundle(lang, namespace, locales[lang][namespace])
      })
    })
    forceUpdate()
  }, [locales])

  const variablesObject = () => {
    return (typeof t('variables')) === 'object' ? t('variables') : {}
  }

  const agreedPrice = []

  const buyerBidsList = clone(buyerBids).sort((a, b) => b.bid - a.bid)
  const sellerBidsList = clone(sellerBids).sort((a, b) => a.bid - b.bid)
  const length = Math.max(...[buyerBidsList, sellerBidsList, deals].map(a => a.length))

  const getTableValue = (map, key) => {
    if (typeof map === 'undefined') return ''
    if (typeof map[key] === 'undefined') return ''

    return map[key]
  }

  for (let i = length - 1; i >= 0; i--) {
    if (deals[i]) {
      agreedPrice.push(
        <TableRow key={i}>
          <TableCell>{t('guest.result.deals_num_01', { i: (length - i) })}</TableCell>
          <TableCell>{getTableValue(deals[i], 'deal')}</TableCell>
        </TableRow>
      )
    }
  }

  if (!(locales))
    return <></>

  return (
    <Table className={classes.bidsTable}>
      <TableHead>
        <TableRow>
          <TableCell>{t('guest.result.deals_num_title_01')}</TableCell>
          <TableCell>{t('guest.result.success_price_01', variablesObject())}</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {agreedPrice}
      </TableBody>
    </Table>
  )
}
