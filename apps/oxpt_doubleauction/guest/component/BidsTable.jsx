import React, { useReducer, useEffect, useState } from 'react'
import { useStore } from '../actions/hook'
import { useTranslation } from 'react-i18next'

import { makeStyles } from '@material-ui/core/styles'

import Table from '@material-ui/core/Table'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import TableCell from '@material-ui/core/TableCell'
import TableBody from '@material-ui/core/TableBody'
import Grid from '@material-ui/core/Grid'
import Switch from '@material-ui/core/Switch'

import clone from 'clone'

import i18nInstance from '../i18n'
import Typography from '@material-ui/core/Typography'

const useStyles = makeStyles(theme => ({
  bidsTable: {
    padding: theme.spacing(1)
  }
}))

export default () => {
  const classes = useStyles()
  const { locales, buyerBids, sellerBids, deals } = useStore()

  const [t, i18n] = useTranslation('translations', { i18nInstance })
  const [, forceUpdate] = useReducer(x => x + 1, 0)

  const [isTable, setIsTable] = useState(false)

  useEffect(() => {
    locales && Object.keys(locales).map(lang => {
      Object.keys(locales[lang]).map(namespace => {
        i18n.addResourceBundle(lang, namespace, locales[lang][namespace])
      })
    })
    forceUpdate()
  }, [locales])

  const variablesObject = () => {
    return (typeof t('variables')) === 'object' ? t('variables') : {}
  }

  const rowsTable = []
  const rowsBoard = []

  const buyerBidsList = clone(buyerBids).sort((a, b) => b.bid - a.bid)
  const sellerBidsList = clone(sellerBids).sort((a, b) => a.bid - b.bid)

  const length = 
    isTable ? Math.max(...[buyerBidsList, sellerBidsList].map(a => a.length))
            : Math.max(...[buyerBidsList, sellerBidsList, deals].map(a => a.length));

  let countsPrice = {}
  let countsPriceBuyer = {}
  let countsPriceSeller = {}

  for (let i = 0; i < buyerBidsList.length; i++) {
    let key = buyerBidsList[i].bid
    countsPriceBuyer[key] = (countsPriceBuyer[key]) ? countsPriceBuyer[key] + 1 : 1
    countsPrice[key] = (countsPrice[key]) ? countsPrice[key] + 1 : 1
  }

  for (let i = 0; i < sellerBidsList.length; i++) {
    let key = sellerBidsList[i].bid
    countsPriceSeller[key] = (countsPriceSeller[key]) ? countsPriceSeller[key] + 1 : 1
    countsPrice[key] = (countsPrice[key]) ? countsPrice[key] + 1 : 1
  }

  let array = []
  for (let key in countsPrice) {
    if (countsPrice.hasOwnProperty(key)) {
      array.push(parseInt(key, 10))
    }
  }

  array.sort(function(a, b) { return b - a })

  const getTableValue = (map, key) => {
    if (typeof map === 'undefined') return ''
    if (typeof map[key] === 'undefined') return ''

    return map[key]
  }

  const handleChange = () => {
    setIsTable(!isTable)
  }

  for (let i = 0; i < length; i++) {
    rowsTable.push(
      <TableRow key={i}>
        <TableCell>{getTableValue(buyerBidsList[i], 'bid')}</TableCell>
        <TableCell>{getTableValue(sellerBidsList[i], 'bid')}</TableCell>
      </TableRow>
    )
  }

  for (let i = 0; i < array.length; i++) {
    rowsBoard.push(
      <TableRow key={i}>
        <TableCell>{getTableValue(countsPriceSeller, array[i])}</TableCell>
        <TableCell>{array[i]}</TableCell>
        <TableCell>{getTableValue(countsPriceBuyer, array[i])}</TableCell>
      </TableRow>
    )
  }

  if (!(locales))
    return <></>

  return (
    <Grid
      container
      direction="column"
      justify="center"
      alignItems="stretch"
    >
      <Grid item>
        <Typography variant="body1" display="inline">{t('guest.result.agreed_price.board')}</Typography>
        <Switch
          checked={isTable}
          onChange={handleChange}
          color="primary"
          name="isTable"
          inputProps={{ 'aria-label': 'Board or Table?' }}
        />
        <Typography variant="body1" display="inline">{t('guest.result.agreed_price.table')}</Typography>
      </Grid>
      <Grid item>
        {isTable &&
        <Table className={classes.bidsTable}>
          <TableHead>
            <TableRow>
              <TableCell>{variablesObject().buying_price_01}</TableCell>
              <TableCell>{variablesObject().selling_price_01}</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rowsTable}
          </TableBody>
        </Table>
        }
        {!isTable &&
          <Table className={classes.bidsTable}>
            <TableHead>
              <TableRow>
                <TableCell>{variablesObject().selling_num_01}</TableCell>
                <TableCell>{variablesObject().price_01}</TableCell>
                <TableCell>{variablesObject().buying_num_01}</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {rowsBoard}
            </TableBody>
          </Table>
        }
      </Grid>
    </Grid>
  )
}
