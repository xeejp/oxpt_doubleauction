import React, { useReducer, useEffect, useState } from 'react'
import { useStore } from '../actions/hook'
import { useTranslation } from 'react-i18next'

import { makeStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'

import CircularProgress from '@material-ui/core/CircularProgress'

import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import Snackbar from '@material-ui/core/Snackbar'

import Dialog from '@material-ui/core/Dialog'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogActions from '@material-ui/core/DialogActions'

import i18nInstance from '../i18n'

const useStyles = makeStyles(theme => ({
  dialogContent: {
    padding: theme.spacing(1),
    align: 'justify'
  },
  items: {
    padding: theme.spacing(1)
  },
  buttonR: {
    paddingLeft: theme.spacing(1),
  },
  buttonL: {
    paddingRight: theme.spacing(1),
  },
  button: {
    padding: theme.spacing(2)
  },
  dialogButton: {
    padding: theme.spacing(1)
  }
}))

export default () => {
  const classes = useStyles()
  const { locales, role, money, bidded, requestState } = useStore()
  if (!(locales))
    return <></>

  const [value, setValue] = useState('')
  const [isValid, setIsValid] = useState(false)
  const [snack, setSnack] = useState(false)
  const [bid, setBid] = useState('')
  const [errorText, setErrorText] = useState('')
  const [openDialog, setOpenDialog] = useState(false)
  const [openBidCancel, setOpenBidCancel] = useState(false)
  const [shakeTF, setShakeTF] = useState(false)
  const [wait, setWait] = useState(false)

  const [t, i18n] = useTranslation('translations', { i18nInstance })
  const [, forceUpdate] = useReducer(x => x + 1, 0)

  useEffect(() => {
    locales && Object.keys(locales).map(lang => {
      Object.keys(locales[lang]).map(namespace => {
        i18n.addResourceBundle(lang, namespace, locales[lang][namespace])
      })
    })
    forceUpdate()
  }, [locales])

  const variablesObject = () => {
    return (typeof t('variables')) === 'object' ? t('variables') : {}
  }

  const setErrorText2 = (role, numValue) => (
    isNaN(numValue)
      ? setErrorText(t('guest.experiment.error_pos_01', variablesObject()))
      : role === 'buyer'
        ? setErrorText(t('guest.experiment.error_low_01', variablesObject()))
        : setErrorText(t('guest.experiment.error_up_01', variablesObject()))
  )

  const handleChange = event => {
    setValue(event.target.value)
    const numValue = parseInt(event.target.value, 10)
    setIsValid(
      role === 'buyer'
        ? numValue <= money && numValue >= 0
        : numValue >= money
    )
    let tempIsValid = role === 'buyer' ? numValue <= money && numValue >= 0 : numValue >= money

    if (tempIsValid) {
      setErrorText('')
    } else if (event.target.value === '') {
      setErrorText(t('guest.experiment.error_non_01'))
    } else {
      setErrorText2(role, numValue)
    }
  }

  const handleOpenDialog = () => {
    setOpenDialog(true)
  }

  const clickBidCancel = () => {
    setOpenBidCancel(true)
  }

  const callbackBid = () => {
    setWait(false)
  }

  const handleBidCancel = () => {
    setIsValid(false)
    setBid('')
    setOpenBidCancel(false)
    setWait(true)
    requestState({
      event: 'bid cancel',
      payload: null,
      successCallback: callbackBid,
      timeoutCallback: callbackBid
    })
  }

  const handleCloseBidCancel = () => {
    setOpenBidCancel(false)
  }

  const shakeTextField = () => {
    setShakeTF(true)
    setTimeout(() => {
      setShakeTF(false)
    }, 500)
  }

  const handleBid = () => {
    setValue('')
    setIsValid(false)
    setSnack(true)
    setBid(value)
    setOpenDialog(false)
    setWait(true)
    requestState({
      event: 'bid',
      payload: parseInt(value, 10),
      successCallback: callbackBid,
      timeoutCallback: callbackBid
    })
  }

  const handleClose = () => {
    setOpenDialog(false)
  }

  const handleKeyDown = event => {
    if (!isValid && (event.key === 'Enter' || event.keyCode === 13)) {
      shakeTextField()
    }
    if (isValid && (event.key === 'Enter' || event.keyCode === 13)) {
      handleOpenDialog()
    }
  }

  const handleBidDialogKeyDown = event => {
    if (isValid && (event.key === 'Enter' || event.keyCode === 13)) {
      handleBid()
    }
  }

  const handleBidCancelDialogKeyDown = event => {
    if (isValid && (event.key === 'Enter' || event.keyCode === 13)) {
      handleBidCancel()
    }
  }

  return (
    <>
      <Grid
        container
        direction="column"
        justify="flex-start"
        alignItems="stretch"
      >
        <Grid item xs={12} sm={12} className={classes.items}>
          <Grid
            container
            direction="row"
            justify="center"
            alignItems="flex-start"
          >
            {shakeTF
              ? <Grid item xs={12} sm={6}>
                <TextField
                  value={value}
                  placeholder={t('guest.experiment.suggest_price_01')}
                  onChange={handleChange}
                  onKeyDown={handleKeyDown}
                  variant="outlined"
                  fullWidth
                  helperText={errorText}
                  className="animated shakeX"
                />
              </Grid>
              : <Grid item xs={12} sm={6}>
                <TextField
                  value={value}
                  placeholder={t('guest.experiment.suggest_price_01')}
                  onChange={handleChange}
                  onKeyDown={handleKeyDown}
                  variant="outlined"
                  fullWidth
                  helperText={errorText}
                />
              </Grid>
            }
          </Grid>
        </Grid>
        <Grid item xs={12} sm={12} className={classes.items}>
          <Grid
            container
            direction="row"
            justify="center"
            alignItems="flex-start"
          >
            <Grid item xs={6} sm={3} className={classes.buttonL}>
              <Button
                color='primary'
                disabled={!isValid || wait}
                onClick={handleOpenDialog}
                fullWidth
                className={classes.button}
                variant="contained"
              >
                {wait && <CircularProgress size={24} className={classes.buttonProgress}/>}
                {!wait && t('guest.experiment.bid_01')}
              </Button>
            </Grid>
            <Grid item xs={6} sm={3} className={classes.buttonR}>
              <Button
                color='secondary'
                disabled={!bidded || wait}
                onClick={clickBidCancel}
                fullWidth
                className={classes.button}
                variant="contained"
                autoFocus
              >
                {wait && <CircularProgress size={24} className={classes.buttonProgress}/>}
                {!wait && t('guest.experiment.bid_cancel_01')}
              </Button>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
      <Dialog
        open={openDialog}
        onClose={handleClose}
        onKeyDown={handleBidDialogKeyDown}
        fullWidth={true}
        maxWidth="sm"
        className={classes.dialogButton}
      >
        <DialogContent className={classes.dialogContent}>
          <DialogContentText>
            {t('guest.experiment.check_bid_01')}
          </DialogContentText>
          <Grid
            container
            direction="row"
            justify="center"
            alignItems="flex-start"
            className={classes.items}
          >
            <Grid item xs={6} sm={4} className={classes.buttonL}>
              <Button
                variant='outlined'
                onClick={handleClose}
                color='secondary'
                className={classes.button}
                fullWidth>
                {t('guest.experiment.cancel_01')}
              </Button>
            </Grid>
            <Grid item xs={6} sm={4} className={classes.buttonR}>
              <Button
                variant='contained'
                onClick={handleBid}
                color='primary'
                className={classes.button}
                fullWidth>
                {t('guest.experiment.bid_01')}
              </Button>
            </Grid>
          </Grid>
        </DialogContent>
      </Dialog>
      {bidded &&
      <Dialog
        open={openBidCancel}
        onClose={handleCloseBidCancel}
        onKeyDown={handleBidCancelDialogKeyDown}
        fullWidth={true}
        maxWidth="sm"
        className={classes.dialogButton}
      >
        <DialogContent className={classes.dialogContent}>
          <DialogContentText>
            {t('guest.experiment.check_bid_cancel_01')}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Grid
            container
            direction="row"
            justify="center"
            alignItems="flex-start"
            className={classes.items}
          >
            <Grid item xs={6} sm={4} className={classes.buttonL}>
              <Button
                variant='outlined'
                onClick={handleCloseBidCancel}
                color='secondary'
                className={classes.button}
                fullWidth>
                {t('guest.experiment.cancel_01')}
              </Button>
            </Grid>
            <Grid item xs={6} sm={4} className={classes.buttonR}>
              <Button
                variant='contained'
                onClick={handleBidCancel}
                color='primary'
                className={classes.button}
                fullWidth
                autoFocus>
                {t('guest.experiment.ok_01')}
              </Button>
            </Grid>
          </Grid>
        </DialogActions>
      </Dialog>
      }
      <Snackbar
        open={snack}
        message={ t('guest.experiment.your_suggestion_01', { ...variablesObject(), bid: bid }) }
        autoHideDuration={1000}
        onClose={setSnack.bind(null, false)}
      />
    </>
  )
}
