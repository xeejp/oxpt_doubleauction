import React, { useReducer, useEffect } from 'react'
import { useStore } from '../actions/hook'
import { useTranslation } from 'react-i18next'

import Grid from '@material-ui/core/Grid'
import SortableTable from './SortableTable'

import i18nInstance from '../i18n'

const Rank = () => {
  const { locales, guestId, players } = useStore()

  const [, i18n] = useTranslation('translations', { i18nInstance })
  const [, forceUpdate] = useReducer(x => x + 1, 0)

  useEffect(() => {
    locales && Object.keys(locales).map(lang => {
      Object.keys(locales[lang]).map(namespace => {
        i18n.addResourceBundle(lang, namespace, locales[lang][namespace])
      })
    })
    forceUpdate()
  }, [locales])

  if (!(locales && players))
    return <></>

  const finishedPlayer = Object.keys(players)

  let data = finishedPlayer
    .map(key => ({
      rank: 1,
      rankSeller: -1,
      rankBuyer: -1,
      key: key,
      profit: players[key].profit,
      role: players[key].role,
    }))
    .sort((a, b) => {
      if (a.profit < b.profit) return 1
      if (a.profit > b.profit) return -1
      return 1
    })

  let rankSel = 1; let rankBuy = 1
  let selHigh = 0; let buyHigh = 0

  let f = true
  // let index
  if (guestId !== null && f && guestId === data[0].key) {
    f = false
    // index = 0
  }

  if (data[0].role === 'seller') {
    selHigh = data[0].profit
    data[0].rankSeller = rankSel
    data[0].rankBuyer = data.length + 1
    rankSel = rankSel + 1
  }
  if (data[0].role === 'buyer') {
    buyHigh = data[0].profit
    data[0].rankBuyer = rankBuy
    data[0].rankSeller = data.length + 1
    rankBuy = rankBuy + 1
  }

  for (let i = 1; i < data.length; i++) {
    if (data[i - 1].profit === data[i].profit) data[i].rank = data[i - 1].rank
    else data[i].rank = i + 1
    if (data[i].role === 'seller') {
      if (rankSel === 1) {
        selHigh = data[i].profit
        data[i].rankSeller = rankSel
        data[i].rankBuyer = data.length + 1
        rankSel = rankSel + 1
      } else {
        if (selHigh === data[i].profit) {
          data[i].rankSeller = rankSel - 1
          data[i].rankBuyer = data.length + 1
        } else {
          selHigh = data[i].profit
          data[i].rankSeller = rankSel
          data[i].rankBuyer = data.length + 1
          rankSel = rankSel + 1
        }
      }
    }
    if (data[i].role === 'buyer') {
      if (rankBuy === 1) {
        buyHigh = data[i].profit
        data[i].rankBuyer = rankBuy
        data[i].rankSeller = data.length + 1
        rankBuy = rankBuy + 1
      } else {
        if (buyHigh === data[i].profit) {
          data[i].rankBuyer = rankBuy - 1
          data[i].rankSeller = data.length + 1
        } else {
          buyHigh = data[i].profit
          data[i].rankBuyer = rankBuy
          data[i].rankSeller = data.length + 1
          rankBuy = rankBuy + 1
        }
      }
    }

    if (guestId !== null && f && guestId === data[i].key) {
      f = false
      // index = i
    }
  }

  return (
    <Grid item xs={12} >
      <SortableTable data={data} guestId={guestId} />
    </Grid>
  )
}

export default Rank
