import React, { useReducer, useEffect } from 'react'
import PropTypes from 'prop-types'
import { useStore } from '../actions/hook'
import { useTranslation } from 'react-i18next'

import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'
import HCExporting from 'highcharts/modules/exporting'

import Grid from '@material-ui/core/Grid'

import i18nInstance from '../i18n'

HCExporting(Highcharts)

const TransitionChart = ({ buyerBids, sellerBids, equiPriceMax, equiPriceMin, volume }) => {
  const { locales, exType, priceInc, priceMax, priceMin, deals, players } = useStore()

  const [t, i18n] = useTranslation('translations', { i18nInstance })
  const [, forceUpdate] = useReducer(x => x + 1, 0)

  useEffect(() => {
    locales && Object.keys(locales).map(lang => {
      Object.keys(locales[lang]).map(namespace => {
        i18n.addResourceBundle(lang, namespace, locales[lang][namespace])
      })
    })
    forceUpdate()
  }, [locales])

  if (!(locales && exType && priceInc && priceMax && priceMin && deals && players))
    return <></>

  const variablesObject = () => {
    return (typeof t('variables')) === 'object' ? t('variables') : {}
  }

  var dealtLog = deals.map(deal => deal.deal)
  const dealtCount = deals.length
  dealtLog.reverse()
  console.log(dealtCount)
  const maxTemp = Math.max(...buyerBids.concat(sellerBids))
  const minTemp = Math.min(...buyerBids.concat(sellerBids))
  const max = Math.floor((maxTemp + (maxTemp - minTemp) / 10))
  const min = Math.floor(Math.max(0, minTemp - (maxTemp - minTemp) / 10))

  return (
    <>
      <Grid container>
        <Grid item xs={12}>
          <HighchartsReact
            options={{
              chart: {
                animation: false,
                inverted: false
              },
              title: {
                text: t('guest.result.chart.price_change_01', variablesObject())
              },
              xAxis: {
                title: {
                  text: t('guest.result.chart.success_order_01')
                },
                min: 0,
                max: dealtCount + 2,
                tickInterval: 1,
                reserved: false,
                plotLines: volume > 0 && [{
                  color: 'black',
                  dashStyle: 'dot',
                  width: 2,
                  value: volume,
                  label: {
                    rotation: 0,
                    y: 15,
                    x: -10,
                    align: 'right',
                    text: t('guest.result.chart.ideal_number_01', { number: volume })
                  },
                  zIndex: 99
                }]
              },
              yAxis: {
                title: {
                  text: variablesObject().price_01
                },
                min: min,
                max: max,
                tickInterval: (exType === 'simple') ? priceInc : Math.floor((priceMax - priceMin) / 10),
                plotLines: [{
                  color: 'black',
                  dashStyle: 'dot',
                  width: 2,
                  value: Math.floor((equiPriceMin + equiPriceMax) * 0.5),
                  label: {
                    align: 'right',
                    x: -10,
                    text: t('guest.result.chart.ideal_price_01', { ...variablesObject(), min: equiPriceMin, max: equiPriceMax })
                  },
                  zIndex: 99
                }]
              },
              plotOptions: {
                area: {
                  fillOpacity: 0.5,
                  marker: {
                    enabled: false
                  }
                }
              },
              series: [{
                type: 'area',
                animation: false,
                name: t('guest.result.success_price_01', variablesObject()),
                data: [null, ...dealtLog]
              }],
              credits: {
                enabled: false
              },
              exporting: {
                buttons: {
                  contextButton: {
                    symbol: 'menu',
                    menuItems: ['downloadPNG']
                  }
                },
                fallbackToExportServer: false
              }
            }}
            highcharts={Highcharts}
          />
        </Grid>
      </Grid>
    </>
  )
}

TransitionChart.propTypes = {
  buyerBids: PropTypes.array,
  sellerBids: PropTypes.array,
  equiPriceMax: PropTypes.number,
  equiPriceMin: PropTypes.number,
  volume: PropTypes.number
}

export default TransitionChart
