import React, { useReducer, useEffect } from 'react'
import { useStore } from '../actions/hook'
import { useTranslation } from 'react-i18next'

import { makeStyles } from '@material-ui/core/styles'

import Typography from '@material-ui/core/Typography'
import indigo from '@material-ui/core/colors/indigo'

import Paper from '@material-ui/core/Paper'

import i18nInstance from '../i18n'

import BidForm from './BidForm'

const useStyles = makeStyles(theme => ({
  items: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1)
  },
  desctiption: {
    marginBottom: theme.spacing(1),
    backgroundColor: indigo[50],
    padding: theme.spacing(1)
  }
}))

export default () => {
  const classes = useStyles()
  const { locales, money, bidded, bid, dealt, deal } = useStore()
  if (!(locales))
    return <></>

  const [t, i18n] = useTranslation('translations', { i18nInstance })
  const [, forceUpdate] = useReducer(x => x + 1, 0)

  useEffect(() => {
    locales && Object.keys(locales).map(lang => {
      Object.keys(locales[lang]).map(namespace => {
        i18n.addResourceBundle(lang, namespace, locales[lang][namespace])
      })
    })
    forceUpdate()
  }, [locales])

  const variablesObject = () => {
    return (typeof t('variables')) === 'object' ? t('variables') : {}
  }

  return (
    <>
      <Paper className={classes.desctiption}>
        {dealt
          ? <Typography
            variant='body1'
            align='justify'
            dangerouslySetInnerHTML={{ __html: t('guest.experiment.success_text_01', { ...variablesObject(), deal: deal, bid: bid, benefit: money - deal }) }}
          />
          : <>
            <Typography
              variant='body1'
              align='justify'
              dangerouslySetInnerHTML={{ __html: t('guest.experiment.your_buyer_01', { ...variablesObject(), money: money }) }}
            />
            { bidded ? <p>{ t('guest.experiment.buyer_suggest_01', { ...variablesObject(), bid: bid }) }</p> : null }
          </>
        }
      </Paper>
      <Paper className={classes.items}>
        {!dealt && <BidForm />}
      </Paper>
    </>
  )
}
