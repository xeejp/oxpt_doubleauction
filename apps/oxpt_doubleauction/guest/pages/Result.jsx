import React, { useReducer, useEffect, useState } from 'react'
import { useStore } from '../actions/hook'
import { useTranslation } from 'react-i18next'

import { makeStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import CardActions from '@material-ui/core/CardActions'
import Collapse from '@material-ui/core/Collapse'
import IconButton from '@material-ui/core/IconButton'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'

import i18nInstance from '../i18n'
import TransitionChart from '../component/TransitionChart'
import CurveChart from '../component/CurveChart'
import Rank from '../component/Rank'
import AgreedPrice from '../component/AgreedPrice'

const useStyles = makeStyles(theme => ({
  items: {
    marginBottom: theme.spacing(2)
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  }
}))

export default () => {
  const classes = useStyles()
  const { locales, players } = useStore()

  const [openTransition, setOpenTransition] = useState(true)
  const [openCurve, setOpenCurve] = useState(false)
  const [openSurplus, setOpenSurplus] = useState(false)
  const [openRank, setOpenRank] = useState(false)
  const [openAgreedPrice, setOpenAgreedPrice] = useState(false)

  const [t, i18n] = useTranslation('translations', { i18nInstance })
  const [, forceUpdate] = useReducer(x => x + 1, 0)

  useEffect(() => {
    locales && Object.keys(locales).map(lang => {
      Object.keys(locales[lang]).map(namespace => {
        i18n.addResourceBundle(lang, namespace, locales[lang][namespace])
      })
    })
    forceUpdate()
  }, [locales])

  if (!(locales && players))
    return <></>

  const variablesObject = () => {
    return (typeof t('variables')) === 'object' ? t('variables') : {}
  }

  const buyerBids = Object.keys(players).filter(guestId => players[guestId].role === 'buyer').map(guestId => players[guestId].money).sort((a, b) => b - a)
  const sellerBids = Object.keys(players).filter(guestId => players[guestId].role === 'seller').map(guestId => players[guestId].money).sort((a, b) => a - b)
  const consumerSurplus = Object.keys(players).filter(guestId => players[guestId].role === 'buyer' && players[guestId].dealt).reduce((acc, guestId) => acc + players[guestId].profit, 0)
  const producerSurplus = Object.keys(players).filter(guestId => players[guestId].role === 'seller' && players[guestId].dealt).reduce((acc, guestId) => acc + players[guestId].profit, 0)
  const totalSurplus = consumerSurplus + producerSurplus

  let i = 0; let volume = 0; let equiPriceMin; let equiPriceMax; let equiPriceMinTemp; let equiPriceMaxTemp
  while (true) {
    if (i >= buyerBids.length && i >= sellerBids.length) break
    if (i < buyerBids.length) equiPriceMaxTemp = buyerBids[i]
    if (i < sellerBids.length) equiPriceMinTemp = sellerBids[i]
    if (equiPriceMaxTemp < equiPriceMinTemp) break
    equiPriceMax = equiPriceMaxTemp
    equiPriceMin = equiPriceMinTemp

    if (i < buyerBids.length && i < sellerBids.length) volume++
    i++
  }

  const consumerTemp = Object.keys(players).filter(guestId => players[guestId].role === 'buyer' && players[guestId].money >= equiPriceMax).reduce((acc, guestId) => acc + players[guestId].money, 0)
  const producerTemp = Object.keys(players).filter(guestId => players[guestId].role === 'seller' && players[guestId].money <= equiPriceMin).reduce((acc, guestId) => acc + players[guestId].money, 0)

  const consumerSurplusMax = consumerTemp - volume * equiPriceMin
  const consumerSurplusMin = consumerTemp - volume * equiPriceMax
  const producerSurplusMin = volume * equiPriceMin - producerTemp
  const producerSurplusMax = volume * equiPriceMax - producerTemp

  const totalSurplusIdeal = consumerSurplusMax + producerSurplusMin

  return (
    <Grid item xs={12}>
      <Card className={classes.items}>
        <CardActions onClick={setOpenTransition.bind(null, !openTransition)}>
          {t('guest.result.title_transition_01', variablesObject())}
          <IconButton
            className={(openTransition ? classes.expandOpen : classes.expand)}
            aria-expanded={openTransition}
            aria-label="Show more"
          >
            <ExpandMoreIcon />
          </IconButton>
        </CardActions>
        <Collapse in={openTransition}>
          <CardContent>
            <TransitionChart
              buyerBids={buyerBids}
              sellerBids={sellerBids}
              equiPriceMax={equiPriceMax}
              equiPriceMin={equiPriceMin}
              volume={volume}
            />
          </CardContent>
        </Collapse>
      </Card>
      <Card className={classes.items}>
        <CardActions onClick={setOpenCurve.bind(null, !openCurve)}>
          {t('guest.result.title_curve_01')}
          <IconButton
            className={(openCurve ? classes.expandOpen : classes.expand)}
            aria-expanded={openCurve}
            aria-label="Show more"
          >
            <ExpandMoreIcon />
          </IconButton>
        </CardActions>
        <Collapse in={openCurve}>
          <CardContent>
            <CurveChart
              buyerBids={buyerBids}
              sellerBids={sellerBids}
              equiPriceMax={equiPriceMax}
              equiPriceMin={equiPriceMin}
              volume={volume}
            />
          </CardContent>
        </Collapse>
      </Card>
      <Card className={classes.items}>
        <CardActions onClick={setOpenSurplus.bind(null, !openSurplus)}>
          {t('guest.result.title_surplus_01')}
          <IconButton
            className={(openSurplus ? classes.expandOpen : classes.expand)}
            aria-expanded={openSurplus}
            aria-label="Show more"
          >
            <ExpandMoreIcon />
          </IconButton>
        </CardActions>
        <Collapse in={openSurplus}>
          <CardContent>
            {
              (equiPriceMax === equiPriceMin)
                ? <p>{t('guest.result.surplus_some_01', { consumer_surplus_max: consumerSurplusMax, producer_surplus_min: producerSurplusMax, total_surplus_ideal: totalSurplusIdeal, ...variablesObject() })}</p>
                : <p>{t('guest.result.surplus_some_01', { consumer_surplus_max: consumerSurplusMax, consumer_surplus_min: consumerSurplusMin, producer_surplus_max: producerSurplusMax, producer_surplus_min: producerSurplusMin, total_surplus_ideal: totalSurplusIdeal, ...variablesObject() })}</p>
            }
            <p>{t('guest.result.surplus_01', { consumer_surplus: consumerSurplus, producer_surplus: producerSurplus, total_surplus: totalSurplus, ...variablesObject() })}</p>
          </CardContent>
        </Collapse>
      </Card>
      <Card className={classes.items}>
        <CardActions onClick={setOpenRank.bind(null, !openRank)}>
          {t('guest.result.title_rank_01')}
          <IconButton
            className={(openRank ? classes.expandOpen : classes.expand)}
            aria-expanded={openRank}
            aria-label="Show more"
          >
            <ExpandMoreIcon />
          </IconButton>
        </CardActions>
        <Collapse in={openRank}>
          <CardContent>
            <Rank />
          </CardContent>
        </Collapse>
      </Card>
      <Card className={classes.items}>
        <CardActions onClick={setOpenAgreedPrice.bind(null, !openAgreedPrice)}>
          {t('guest.result.title_bids_table_01')}
          <IconButton
            className={(openAgreedPrice ? classes.expandOpen : classes.expand)}
            aria-expanded={openAgreedPrice}
            aria-label="Show more"
          >
            <ExpandMoreIcon />
          </IconButton>
        </CardActions>
        <Collapse in={openAgreedPrice}>
          <CardContent>
            <AgreedPrice />
          </CardContent>
        </Collapse>
      </Card>
    </Grid>
  )
}
