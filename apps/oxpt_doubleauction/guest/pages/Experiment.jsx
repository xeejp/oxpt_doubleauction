import React, { useReducer, useEffect, useState } from 'react'
import { useStore } from '../actions/hook'
import { useTranslation } from 'react-i18next'

import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'

import i18nInstance from '../i18n'

import Buyer from '../component/Buyer'
import Seller from '../component/Seller'
import BidsTable from '../component/BidsTable'
import AgreedPrice from '../component/AgreedPrice'
import Notice from '../component/Notice'

import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles(theme => ({
  items: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1)
  }
}))

export default () => {
  const classes = useStyles()
  const { locales, role, money, dealt } = useStore()
  if (!(locales))
    return <></>

  const [t, i18n] = useTranslation('translations', { i18nInstance })
  const [, forceUpdate] = useReducer(x => x + 1, 0)
  const [startFlag, setStartFlag] = useState(!dealt)

  const handleCloseStart = () => {
    setStartFlag(false)
  }

  const variablesObject = () => {
    return (typeof t('variables')) === 'object' ? t('variables') : {}
  }

  useEffect(() => {
    locales && Object.keys(locales).map(lang => {
      Object.keys(locales[lang]).map(namespace => {
        i18n.addResourceBundle(lang, namespace, locales[lang][namespace])
      })
    })
    forceUpdate()
  }, [locales])

  return (
    <>
      {
        role === 'buyer'
          ? <Buyer />
          : role === 'seller'
            ? <Seller />
            : <p>{ t('donot_join') }</p>
      }
      <Card className={classes.items}>
        <CardContent>
          <BidsTable />
        </CardContent>
      </Card>
      <Card className={classes.items}>
        <CardContent>
          <AgreedPrice />
        </CardContent>
      </Card>
      <Notice
        open={startFlag}
        handleClose={handleCloseStart}
        message={t('guest.experiment.your_' + role + '_n_01', { ...variablesObject(), money: money })}
      />
    </>
  )
}
