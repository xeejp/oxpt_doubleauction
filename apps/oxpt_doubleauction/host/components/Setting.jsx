import React, { useReducer, useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import { useStore } from '../actions/hook'
import { useTranslation } from 'react-i18next'

import { makeStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'

import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableContainer from '@material-ui/core/TableContainer'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Paper from '@material-ui/core/Paper'

import AppBar from '@material-ui/core/AppBar'
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'
import Box from '@material-ui/core/Box'

import Dialog from '@material-ui/core/Dialog'
import DialogTitle from '@material-ui/core/DialogTitle'
import DialogContent from '@material-ui/core/DialogContent'
import DialogActions from '@material-ui/core/DialogActions'
import Button from '@material-ui/core/Button'
import IconButton from '@material-ui/core/IconButton'
import TextField from '@material-ui/core/TextField'
import Select from '@material-ui/core/Select'
import MenuItem from '@material-ui/core/MenuItem'

import i18nInstance from '../i18n'

import Translate from '@material-ui/icons/Translate'
import Settings from '@material-ui/icons/Settings'
import CustomChart from './CustomChart'

const useStyles = makeStyles(theme => ({
  root: {
    padding: 0,
    height: '100vh',
    overflow: 'auto',
    margin: theme.spacing(1)
  },
  dialog: {
    height: '80vh',
  },
  dialogTitle: {
    margin: 0,
    padding: 0
  },
  formControl: {
    margin: theme.spacing(1),
  },
  group: {
    margin: theme.spacing(1, 0),
  },
}))

const TabPanel = (props) => {
  const { children, value, index, ...other } = props

  return (
    <Typography
      component="div"
      role="tabpanel"
      hidden={value !== index}
      id={`scrollable-force-tabpanel-${index}`}
      aria-labelledby={`scrollable-force-tab-${index}`}
      {...other}
    >
      {value === index && <Box p={3}>{children}</Box>}
    </Typography>
  )
}

const Setting = () => {
  const classes = useStyles()
  const { locales, page, players, requestState, exType, priceBase, priceInc, priceMax, priceMin, buyerPrice, sellerPrice } = useStore()
  if (!(locales && exType && priceBase && priceInc && priceMax && priceMin))
    return <></>

  const [exTypeVar, setexTypeVar] = useState(exType)
  const [priceBaseVar, setpriceBaseVar] = useState(priceBase)
  const [priceIncVar, setpriceIncVar] = useState(priceInc)
  const [priceMaxVar, setpriceMaxVar] = useState(priceMax)
  const [priceMinVar, setpriceMinVar] = useState(priceMin)
  const [customBuyerPrice, setCustomBuyerPrice] = useState((buyerPrice !== null) ? buyerPrice : '')
  const [customBuyerPriceText, setCustomBuyerPriceText] = useState((buyerPrice !== null) ? buyerPrice.join(', ') : null)
  const [customSellerPrice, setCustomSellerPrice] = useState((sellerPrice !== null) ? sellerPrice : '')
  const [customSellerPriceText, setCustomSellerPriceText] = useState((sellerPrice !== null) ? sellerPrice.join(', ') : null)
  const [validBuyerPrice, setValidBuyerPrice] = useState(true)
  const [errorBuyerText, setErrorBuyerText] = useState('')
  const [validSellerPrice, setValidSellerPrice] = useState(true)
  const [errorSellerText, setErrorSellerText] = useState('')
  const [customValid, setCustomValid] = useState(true)
  const [customErrorText, setCustomErrorText] = useState('')
  const [valid, setValid] = useState(true)

  const languageVariablesObject = (key) => {
    return (typeof locales[key].translations.variables) === 'object' ? locales[key].translations.variables : {}
  }

  const langKeys = Object.keys(locales)
  const initUnits = langKeys.reduce((units, key) => ({
    ...units,
    [key]: languageVariablesObject(key)
  }), {})
  const [open, setOpen] = useState(page === 'instruction' || page === 'waiting')
  var localesTempStr = JSON.stringify(initUnits)
  var localesTemp = JSON.parse(localesTempStr)

  const [t, i18n] = useTranslation('translations', { i18nInstance })
  const [, forceUpdate] = useReducer(x => x + 1, 0)
  const [value, setValue] = useState(0)

  useEffect(() => {
    locales && Object.keys(locales).map(lang => {
      Object.keys(locales[lang]).map(namespace => {
        i18n.addResourceBundle(lang, namespace, locales[lang][namespace])
      })
    })
    forceUpdate()
  }, [locales])

  const handleChangeexTypeVar = e => {
    const temp = e.target.value
    setexTypeVar(temp)
    onChangeSetValid({ ex_type: temp, priceBaseVar, priceIncVar, priceMaxVar, priceMinVar, validBuyerPrice, validSellerPrice, customBuyerPrice, customSellerPrice })
  }

  const handleChangepriceBaseVar = e => {
    const temp = parseInt(e.target.value)
    setpriceBaseVar(temp)
    onChangeSetValid({ exTypeVar, price_base: temp, priceIncVar, priceMaxVar, priceMinVar, validBuyerPrice, validSellerPrice, customBuyerPrice, customSellerPrice })
  }

  const handleChangepriceIncVar = e => {
    const temp = parseInt(e.target.value)
    setpriceIncVar(temp)
    onChangeSetValid({ exTypeVar, priceBaseVar, price_inc: temp, priceMaxVar, priceMinVar, validBuyerPrice, validSellerPrice, customBuyerPrice, customSellerPrice })
  }

  const handleChangepriceMaxVar = e => {
    const temp = parseInt(e.target.value)
    setpriceMaxVar(temp)
    onChangeSetValid({ exTypeVar, priceBaseVar, priceIncVar, price_max: temp, priceMinVar, validBuyerPrice, validSellerPrice, customBuyerPrice, customSellerPrice })
  }

  const handleChangepriceMinVar = e => {
    const temp = parseInt(e.target.value)
    setpriceMinVar(temp)
    onChangeSetValid({ exTypeVar, priceBaseVar, priceIncVar, priceMaxVar, price_min: temp, validBuyerPrice, validSellerPrice, customBuyerPrice, customSellerPrice })
  }

  const handleChangeCustomBuyerPriceText = e => {
    const temp = e.target.value
    setCustomBuyerPriceText(temp)

    const buyerPrices = temp.trim().split(',').filter((price, index, arr) => arr.length - 1 !== index || price !== '').map(price => parseInt(price))
    const buyerValid = buyerPrices.every(price => !isNaN(price) && price > 0)
    if (buyerValid) {
      setErrorBuyerText('')
      setValidBuyerPrice(true)
      setCustomBuyerPrice((buyerPrices !== null) ? buyerPrices : [])
    } else {
      setErrorBuyerText(t('host.setting.custom.error_price_01'))
      setValidBuyerPrice(false)
    }

    onChangeSetValid({ exTypeVar, priceBaseVar, priceIncVar, priceMaxVar, priceMinVar, validBuyerPrice: buyerValid, validSellerPrice, customBuyerPrice: buyerPrices, customSellerPrice })
  }

  const handleChangeCustomSellerPriceText = e => {
    const temp = e.target.value
    setCustomSellerPriceText(temp)

    const sellerPrices = temp.trim().split(',').filter((price, index, arr) => arr.length - 1 !== index || price !== '').map(price => parseInt(price))
    const sellerValid = sellerPrices.every(price => !isNaN(price) && price > 0)
    if (sellerValid) {
      setErrorSellerText('')
      setValidSellerPrice(true)
      setCustomSellerPrice((sellerPrices !== null) ? sellerPrices : [])
    } else {
      setErrorSellerText(t('host.setting.custom.error_price_01'))
      setValidSellerPrice(false)
    }

    onChangeSetValid({ exTypeVar, priceBaseVar, priceIncVar, priceMaxVar, priceMinVar, validBuyerPrice, validSellerPrice: sellerValid, customBuyerPrice, customSellerPrice: sellerPrices })
  }

  const onChangeSetValid = ({ exTypeVar, priceBaseVar, priceIncVar, priceMaxVar, priceMinVar, validBuyerPrice, validSellerPrice, customBuyerPrice, customSellerPrice }) => {
    if (exTypeVar === 'simple') {
      setValid(priceBaseVar >= 0 && priceIncVar > 0)
    } else if (exTypeVar === 'real') {
      setValid(priceMinVar >= 0 && priceMaxVar > priceMinVar)
    } else {
      if (customBuyerPrice.length === 0 || customSellerPrice.length === 0) {
        setCustomErrorText(t('host.setting.custom.error_price_zero_01'))
        setValid(false)
        setCustomValid(false)
      } else if (customBuyerPrice.length + customSellerPrice.length < Object.keys(players).length) {
        setCustomErrorText(t('host.setting.custom.error_price_number_01'))
        setValid(false)
        setCustomValid(false)
      } else {
        setCustomErrorText('')
        setValid(true)
        setValid(validBuyerPrice && validSellerPrice)
      }
    }
  }

  const handleVariableUnits = (obj, e) => {
    var tempvar = obj.var
    localesTemp[obj.lang][tempvar] = e.target.value
  }

  const handleOnOpen = () => {
    onChangeSetValid({ exTypeVar, priceBaseVar, priceIncVar, priceMaxVar, priceMinVar, validBuyerPrice, validSellerPrice, customBuyerPrice, customSellerPrice })
    setOpen(true)
  }

  const handleOnCancel = () => {
    setValue(0)
    setexTypeVar(exTypeVar)
    setpriceBaseVar(priceBaseVar)
    setpriceIncVar(priceIncVar)
    setpriceMaxVar(priceMaxVar)
    setpriceMinVar(priceMinVar)
    setCustomBuyerPrice((buyerPrice !== null) ? buyerPrice : '')
    setCustomBuyerPriceText((buyerPrice !== null) ? buyerPrice.join(', ') : null)
    setCustomSellerPrice((sellerPrice !== null) ? sellerPrice : '')
    setCustomSellerPriceText((sellerPrice !== null) ? sellerPrice.join(', ') : null)
    setOpen(false)
  }

  const handleOnSend = () => {
    requestState({
      event: 'setting',
      payload: {
        ex_type: exTypeVar,
        price_base: priceBaseVar,
        price_inc: priceIncVar,
        price_max: priceMaxVar,
        price_min: priceMinVar,
        buyerPrice: customBuyerPrice,
        sellerPrice: customSellerPrice,
        locales_temp: localesTemp
      },
      successCallback: () => {},
      timeoutCallback: () => {}
    })
    setOpen(false)
  }

  const handleChange = (event, newValue) => {
    setValue(newValue)
  }

  const a11yProps = (index) => {
    return {
      id: `scrollable-force-tab-${index}`,
      'aria-controls': `scrollable-force-tabpanel-${index}`,
    }
  }

  return (
    <div
      className={classes.root}
    >
      <Grid item xs={12}>
        <IconButton color="primary" aria-label={t('host.setting.title_01')} disabled={page !== 'instruction'} onClick={handleOnOpen}>
          <Settings fontSize="large" />
        </IconButton>
        <Dialog onClose={handleOnCancel} open={open} fullWidth={true} maxWidth="md">
          <DialogTitle>
            <AppBar position="relative" color="default">
              <Tabs
                value={value}
                onChange={handleChange}
                variant="scrollable"
                scrollButtons="on"
                indicatorColor="primary"
                textColor="primary"
                aria-label="scrollable force tabs"
              >
                <Tab label={t('host.setting.game.title_01')} icon={<Settings />} {...a11yProps(0)} />
                <Tab label={t('host.setting.language.title_01')} icon={<Translate />} {...a11yProps(1)} />
                <Tab label={t('host.setting.custom.title_01')} icon={<Settings />} {...a11yProps(2)} disabled={exTypeVar !== 'custom'}/>
              </Tabs>
            </AppBar>
          </DialogTitle>
          <DialogContent>
            <TabPanel value={value} index={0}>
              <TableContainer component={Paper}>
                <Table aria-label="setting table" size="small">
                  <TableHead>
                    <TableRow>
                      <TableCell align="left">{t('host.setting.game.header_item_01')}</TableCell>
                      <TableCell align="center">{t('host.setting.game.header_info_01')}</TableCell>
                      <TableCell align="center">{t('host.setting.game.setting_01')}</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    <TableRow>
                      <TableCell component="th" scope="row" align="left">
                        {t('host.setting.game.ex_type_setting_01')}
                      </TableCell>
                      <TableCell scope="row" align="right">
                        {t(`host.setting.game.${exTypeVar}_01`)}
                      </TableCell>
                      <TableCell align="center">
                        <Select
                          value={exTypeVar}
                          onChange={handleChangeexTypeVar}
                        >
                          <MenuItem value={'simple'}>{t('host.setting.game.simple_01')}</MenuItem>
                          <MenuItem value={'real'}>{t('host.setting.game.real_01')}</MenuItem>
                          <MenuItem value={'custom'}>{t('host.setting.game.custom_01')}</MenuItem>
                        </Select>
                      </TableCell>
                    </TableRow>
                    {(exTypeVar === 'simple')
                      ? <TableRow>
                        <TableCell component="th" scope="row" align="left">
                          {t('host.setting.game.price_base_01')}
                        </TableCell>
                        <TableCell scope="row" align="right">
                          {priceBaseVar}
                        </TableCell>
                        <TableCell align="center">
                          <TextField
                            type='number'
                            value={priceBaseVar}
                            onChange={handleChangepriceBaseVar}
                          />
                        </TableCell>
                      </TableRow>
                      : (exTypeVar === 'real')
                        ? <TableRow>
                          <TableCell component="th" scope="row" align="left">
                            {t('host.setting.game.price_min_01')}
                          </TableCell>
                          <TableCell scope="row" align="right">
                            {priceMinVar}
                          </TableCell>
                          <TableCell align="center">
                            <TextField
                              type='number'
                              value={priceMinVar}
                              onChange={handleChangepriceMinVar}
                            />
                          </TableCell>
                        </TableRow>
                        : null
                    }
                    {(exTypeVar === 'simple')
                      ? <TableRow>
                        <TableCell component="th" scope="row" align="left">
                          {t('host.setting.game.price_inc_01')}
                        </TableCell>
                        <TableCell scope="row" align="right">
                          {priceIncVar}
                        </TableCell>
                        <TableCell align="center">
                          <TextField
                            type='number'
                            value={priceIncVar}
                            onChange={handleChangepriceIncVar}
                          />
                        </TableCell>
                      </TableRow>
                      : (exTypeVar === 'real')
                        ? <TableRow>
                          <TableCell component="th" scope="row" align="left">
                            {t('host.setting.game.price_max_01')}
                          </TableCell>
                          <TableCell scope="row" align="right">
                            {priceMaxVar}
                          </TableCell>
                          <TableCell align="center">
                            <TextField
                              type='number'
                              value={priceMaxVar}
                              onChange={handleChangepriceMaxVar}
                            />
                          </TableCell>
                        </TableRow>
                        : null
                    }
                  </TableBody>
                </Table>
              </TableContainer>
            </TabPanel>
            <TabPanel value={value} index={1}>
              <TableContainer component={Paper}>
                <Table aria-label="setting table" size="small">
                  <TableHead>
                    <TableRow>
                      {Object.keys(initUnits).map(k => {
                        return <TableCell align="center" key={k}>
                          {t('host.setting.language.' + k + '_01')}
                        </TableCell>
                      })}
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {Object.keys(initUnits['en']).map((key, index) => {
                      return <TableRow key={key + '_' + index}>
                        {Object.keys(initUnits).map(k => {
                          return <TableCell align="center" key={key + '_' + index + '_' + k}>
                            <TextField
                              key={key + '_' + index + '_' + k}
                              defaultValue={Object.values(initUnits[k])[index] || null}
                              onChange={handleVariableUnits.bind(null, { lang: k, var: key })}
                            />
                          </TableCell>
                        })}
                      </TableRow>
                    })}
                  </TableBody>
                </Table>
              </TableContainer>
            </TabPanel>
            <TabPanel value={value} index={2}>
              <TableContainer component={Paper}>
                <Table aria-label="setting table" size="small">
                  <TableHead>
                    <TableRow>
                      <TableCell align="left">{t('host.setting.game.header_item_01')}</TableCell>
                      <TableCell align="center">{t('host.setting.game.setting_01')}</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    <TableRow>
                      <TableCell align="left">{t('host.setting.custom.demand_01')}</TableCell>
                      <TableCell align="center">
                        <TextField
                          fullWidth
                          value={customBuyerPriceText}
                          onChange={handleChangeCustomBuyerPriceText}
                        />
                        <br/>
                        {!validBuyerPrice && (
                          <p style={{ color: 'red' }}>{errorBuyerText}</p>
                        )}
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell align="left">{t('host.setting.custom.supply_01')}</TableCell>
                      <TableCell align="center">
                        <TextField
                          fullWidth
                          value={customSellerPriceText}
                          onChange={handleChangeCustomSellerPriceText}
                        />
                        <br/>
                        {!validSellerPrice && (
                          <p style={{ color: 'red' }}>{errorSellerText}</p>
                        )}
                      </TableCell>
                    </TableRow>
                  </TableBody>
                </Table>
              </TableContainer>
              <p>{t('host.setting.custom.players_01', { players_num: Object.keys(players).length, setting_num: customBuyerPrice.length + customSellerPrice.length })}</p>
              {!customValid && (
                <p style={{ color: 'red' }}>{customErrorText}</p>
              )}
              <CustomChart
                buyerBids={customBuyerPrice}
                sellerBids={customSellerPrice}
              />
            </TabPanel>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleOnCancel}>
              {t('host.setting.cancel_01')}
            </Button>
            <Button onClick={handleOnSend} color="primary" disabled={page !== 'instruction'}>
              {t('host.setting.send_01')}
            </Button>
          </DialogActions>
        </Dialog>
      </Grid>
    </div>
  )
}

Setting.propTypes = {
  value: PropTypes.number,
  index: PropTypes.number
}

TabPanel.propTypes = {
  value: PropTypes.number,
  index: PropTypes.number,
  children: PropTypes.node
}

export default Setting
