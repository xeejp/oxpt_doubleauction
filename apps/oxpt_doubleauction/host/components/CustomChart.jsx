import React, { useReducer, useEffect } from 'react'
import PropTypes from 'prop-types'
import { useStore } from '../actions/hook'
import { useTranslation } from 'react-i18next'

import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'
import HCExporting from 'highcharts/modules/exporting'

import Grid from '@material-ui/core/Grid'

import i18nInstance from '../i18n'

HCExporting(Highcharts)

const CustomChart = ({ buyerBids, sellerBids }) => {
  const { locales } = useStore()

  const [t, i18n] = useTranslation('translations', { i18nInstance })
  const [, forceUpdate] = useReducer(x => x + 1, 0)

  useEffect(() => {
    locales && Object.keys(locales).map(lang => {
      Object.keys(locales[lang]).map(namespace => {
        i18n.addResourceBundle(lang, namespace, locales[lang][namespace])
      })
    })
    forceUpdate()
  }, [locales])

  if (!locales)
    return <></>

  const variablesObject = () => {
    return (typeof t('variables')) === 'object' ? t('variables') : {}
  }

  const buyerSorted = [...buyerBids].sort((a, b) => b - a)
  const sellerSorted = [...sellerBids].sort((a, b) => a - b)
  const maxTemp = Math.max(...[...buyerBids, ...sellerBids])
  const minTemp = Math.min(...[...buyerBids, ...sellerBids])
  const max = Math.floor((maxTemp + (maxTemp - minTemp) / 10))
  const min = Math.floor(Math.max(0, minTemp - (maxTemp - minTemp) / 10))

  let i = 0; let volume = 0; let equiPriceMin; let equiPriceMax; let equiPriceMinTemp; let equiPriceMaxTemp
  while (true) {
    if (i >= buyerBids.length && i >= sellerBids.length) break
    if (i < buyerBids.length) equiPriceMaxTemp = buyerSorted[i]
    if (i < sellerBids.length) equiPriceMinTemp = sellerSorted[i]
    if (equiPriceMaxTemp < equiPriceMinTemp) break
    equiPriceMax = equiPriceMaxTemp
    equiPriceMin = equiPriceMinTemp

    if (i < buyerBids.length && i < sellerBids.length) volume++
    i++
  }

  return (
    <>
      <Grid container>
        <Grid item xs={12}>
          <HighchartsReact
            options = {{
              chart: {
                type: 'area',
                animation: false,
                inverted: true
              },
              title: {
                text: t('guest.result.chart.chart_title_01')
              },
              xAxis: {
                title: {
                  text: variablesObject().price_01
                },
                min: min,
                max: max,
                tickInterval: Math.max(1, Math.floor((max - min) / 10)),
                reversed: false,
                plotLines: volume > 0 && [{
                  color: 'black',
                  dashStyle: 'dot',
                  width: 2,
                  value: Math.floor((equiPriceMin + equiPriceMax) * 0.5),
                  label: {
                    align: 'right',
                    x: -10,
                    text: t('guest.result.chart.ideal_price_01', { ...variablesObject(), min: equiPriceMin, max: equiPriceMax })
                  },
                  zIndex: 99
                }]
              },
              yAxis: {
                title: {
                  text: t('guest.result.chart.number_01')
                },
                min: 0,
                max: Math.max(buyerBids.length, sellerBids.length) + 1,
                tickInterval: 1,
                plotLines: [{
                  color: 'black',
                  dashStyle: 'dot',
                  width: 2,
                  value: volume,
                  label: {
                    rotation: 0,
                    y: 15,
                    x: 10,
                    text: t('guest.result.chart.ideal_number_01', { number: volume })
                  },
                  zIndex: 99
                }]
              },
              plotOptions: {
                area: {
                  fillOpacity: 0.5,
                  marker: {
                    enabled: false
                  }
                }
              },
              series: [
                {
                  animation: false,
                  name: t('guest.result.chart.demand_01'),
                  step: 'right',
                  data: [[min, buyerBids.length], ...buyerBids.sort((a, b) => a - b).map((money, index) => [money, buyerBids.length - index]), [max, 0]]
                },
                {
                  animation: false,
                  name: t('guest.result.chart.supply_01'),
                  step: 'left',
                  data: [[min, 0], ...sellerBids.sort((a, b) => a - b).map((money, index) => [money, index + 1]), [max, sellerBids.length]]
                }],
              credits: {
                enabled: false
              },
              exporting: {
                buttons: {
                  contextButton: {
                    symbol: 'menu',
                    menuItems: ['downloadPNG']
                  }
                },
                fallbackToExportServer: false
              }
            }}
            highcharts={Highcharts}
          />
        </Grid>
      </Grid>
    </>
  )
}

CustomChart.propTypes = {
  buyerBids: PropTypes.any,
  sellerBids: PropTypes.any
}

export default CustomChart
